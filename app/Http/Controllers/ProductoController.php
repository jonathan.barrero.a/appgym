<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Unidades;
use App\Categoria;

use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        if($search)
        {
            $query = '%' . $search . '%';
            $productos = Producto::where('nombre', 'like', $query)->orderBy('id')->paginate(10);
        }else{
            $productos = Producto::orderBy('id')->paginate(10);
        }

        return view('admin.producto.index', compact('productos', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades = Unidades::all();
        $categorias = Categoria::all();
    
        return view('admin.producto.create', compact('unidades', 'categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $messages = [
            'nombre.required' => 'Se requiere un nombre para el producto.',
            'codigo_producto.required' => 'Se requieres un codigo del producto para identificarlo.',
            'precio_costo.required' => 'Se requiere un precio de compra.',
            'precio_venta.required' => 'Se requiere un precio de venta.',
            'stock.required' => 'Se requiere cantidad de stock para el producto.',
            'stock_min.required' => 'Se requiere un stock minimo para realizar la alerta.',
            'categoria_id.required' => 'Se requiere una categoria.',
            'unidades_id.required' => 'Se requiere una unidad para el producto.'            
        ];
        $rules = [
            'nombre' => 'required',
            'codigo_producto' => 'required',
            'precio_costo' => 'required',
            'precio_venta' => 'required',
            'stock' => 'required',
            'stock_min' => 'required',
            'categoria_id' => 'required',
            'unidades_id' => 'required'
        ];
        $this->validate($request, $messages, $rules);

        //$data = $request->all();
        //$data['user_id'] = auth()->user()->id;

        //Producto::create($data);

        $request->merge(['user_id' => auth()->user()->id]);        
        Producto::create($request->all());             
        //Producto::create($request->only(['nombre', 'codigo_producto', 'precio_costo', 'precio_venta', 'stock', 'stock_min', 'categoria_id', 'unidades_id', 'user_id' => auth()->user()->name]));

        return redirect('/admin/producto');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        $unidades = Unidades::all();
        $categorias = Categoria::all();

        return view('admin.producto.edit', compact('unidades', 'categorias', 'producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {
        $messages = [
            'nombre.required' => 'Se requiere un nombre para el producto.',
            'codigo_producto.required' => 'Se requieres un codigo del producto para identificarlo.',
            'precio_costo.required' => 'Se requiere un precio de compra.',
            'precio_venta.required' => 'Se requiere un precio de venta.',
            'stock.required' => 'Se requiere cantidad de stock para el producto.',
            'stock_min.required' => 'Se requiere un stock minimo para realizar la alerta.',
            'categoria_id.required' => 'Se requiere una categoria.',
            'unidades_id.required' => 'Se requiere una unidad para el producto.'            
        ];
        $rules = [
            'nombre' => 'required',
            'codigo_producto' => 'required',
            'precio_costo' => 'required',
            'precio_venta' => 'required',
            'stock' => 'required',
            'stock_min' => 'required',
            'categoria_id' => 'required',
            'unidades_id' => 'required'
        ];
        $this->validate($request, $messages, $rules);

        
        $request->merge(['user_id' => auth()->user()->id]); 
        $producto->update($request->all());
        return redirect('/admin/producto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        return back();
    }
}
