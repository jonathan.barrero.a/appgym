<?php

namespace App\Http\Controllers;

use App\Incidencia;
use Illuminate\Http\Request;

class IncidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge(['instructor_id' => $request->incidencia_id]);
        Incidencia::create($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $FechaInicio = $request->Fecha_inicio;
        $FechaFin = $request->Fecha_Fin;
        
        if($FechaInicio)
        {            
            $incidencias = Incidencias::join('instructors', 'incidencias.instructor_id', '=', 'instructors.id')
                                       ->select('instructors.*', 'incidencias.*')
                                       ->whereBetween('incidencias.created_at', [$FechaInicio, $FechaFin])    
                                       ->orderBy('incidencias.id', 'desc')
                                       ->paginate(10);            
        }else
        {
            $incidencias = Incidencia::get();            
        }

        return view('admin.incidencias.show', compact('incidencias'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Incidencia $incidencia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incidencia $incidencia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incidencia $incidencia)
    {
        //
    }
}
