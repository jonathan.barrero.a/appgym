<?php

namespace App\Http\Controllers;

use App\Compra;
use App\DetalleCompra;
use App\Producto;
use Illuminate\Http\Request;
use App\Exports\ComprasExport;

class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ModelDetalleCompra = new DetalleCompra();
        $productos = Producto::get();
        $detallecompras = DetalleCompra::getDetalle();
        $objetoCompras = Compra::where('estado', '=', 'Pendiente')->select('id')->first();        

        ($objetoCompras) ? $idCompras = $objetoCompras->id : $idCompras = 0;          
        $total = $ModelDetalleCompra->getTotal($detallecompras);

        return view('admin.compras.index', compact('detallecompras', 'productos', 'idCompras', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idCompra = $request->compra_id;
        $compra = Compra::find($idCompra);
        $compra->estado = "Pagado";
        $compra->save();
        $stocks = DetalleCompra::where('compra_id', '=', $idCompra)->get();
        foreach($stocks as $stock)
        {
            $producto = Producto::find($stock->producto_id);            
            $producto->stock = $producto->stock + $stock->cantidad;
            $producto->save();
        }
        $notification = 'La compra fue realizada.';
        return back()->with(compact('notification'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $FechaInicio = $request->Fecha_inicio;
        $FechaFin = $request->Fecha_Fin;

        if($FechaInicio)
        {            

            $detallecompras = DetalleCompra::join('compras', 'detalle_compras.compra_id', '=', 'compras.id')
                                          ->where('compras.estado', '=', 'Pagado')
                                          ->select('detalle_compras.*', 'compras.user_id')
                                          ->whereBetween('compras.created_at', [$FechaInicio, $FechaFin])    
                                          ->orderBy('detalle_compras.id', 'desc')
                                          ->get();                                    
        }else{                        
            $detallecompras = DetalleCompra::getDetalleCompras();                     
        }
        return view('admin.compras.show', compact('detallecompras', 'FechaInicio', 'FechaFin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compra $compra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compra $compra)
    {
        //
    }
    public function export($dateInit, $dateEnd)
    {
        $dateIni = $dateInit;
        $dateFin = $dateEnd;

        return (new ComprasExport($dateIni, $dateFin))->download('compras.xlsx');
    }
}
