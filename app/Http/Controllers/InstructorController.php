<?php

namespace App\Http\Controllers;

use App\Instructor;
use App\Disciplina;
use Illuminate\Http\Request;
use App\Exports\InstructorExport;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        if($search)
        {
            $query = '%' . $search . '%';
            $instructors = Instructor::where('Nombres', 'like', $query)->orderBy('id')->paginate(10);
        }else{
            $instructors = Instructor::orderBy('id')->paginate(10);
        }

        
        
        return view('admin.Instructor.index', compact('instructors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disciplinas = Disciplina::all();
        return view('admin.Instructor.create', compact('disciplinas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Nombres.required' => 'Es necesario Cargar un Nombre.',
            'Nombres.min' => 'Campo Nombres necesita minimo de 3 caracteres.',
            'Apellido_Paterno.required' => 'Es necesario Apellido Paterno',
            'Apellido_Paterno.min' => 'Campo Apellido Paterno minimo de 3 caracteres.',
            'Apellido_Materno.required' => 'Es necesario Apellido Materno',
            'Apellido_Materno.min' => 'Campo Apellido Materno minimo de 3 caracteres.',
            'Tipo_pago.required' => 'Es necesario una opcion de forma de pago.',
            'disciplina_id.required' => 'Es necesario una Disciplina.'
        ];
        $rules = [
            'Nombres' => 'required|min:3',
            'Apellido_Paterno' => 'required|min:3',
            'Apellido_Materno' => 'required|min:3',
            'Tipo_pago' => 'required',
            'disciplina_id' => 'required'
        ];

        $this->validate($request, $rules, $messages);

        Instructor::create($request->all());
        return redirect('/admin/Instructor');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)    
    {
        $FechaInicio = $request->Fecha_inicio;
        $FechaFin = $request->Fecha_Fin;
        $idInstructor = $request->instructor_id;
        $totalInstructores = Instructor::get();
        if($FechaInicio)
        {
            $instructors = Instructor::join('disciplinas', 'instructors.disciplina_id', '=', 'disciplinas.id')
                                      ->join('incripcions', 'disciplinas.id', '=', 'incripcions.disciplina_id')
                                      ->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')
                                      ->select('clientes.*', 'disciplinas.*', 'incripcions.*', 'instructors.*')
                                      ->where('instructors.id', '=', $idInstructor)    
                                      ->whereBetween('incripcions.created_at', [$FechaInicio, $FechaFin])
                                      ->orderBy('instructors.id', 'desc')
                                      ->paginate(10);
                                 
        }else{
            $instructors = [];
        }
        
        return view('admin.Instructor.show', compact('instructors', 'FechaInicio', 'FechaFin', 'totalInstructores', 'idInstructor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function edit(Instructor $instructor)
    {
        $disciplinas = Disciplina::all();
        
        return view('admin.Instructor.edit', compact('instructor', 'disciplinas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instructor $instructor)
    {           
        $messages = [
            'Nombres.required' => 'Es necesario Cargar un Nombre.',
            'Nombres.min' => 'Campo Nombres necesita minimo de 3 caracteres.',
            'Apellido_Paterno.required' => 'Es necesario Apellido Paterno',
            'Apellido_Paterno.min' => 'Campo Apellido Paterno minimo de 3 caracteres.',
            'Apellido_Materno.required' => 'Es necesario Apellido Materno',
            'Apellido_Materno.min' => 'Campo Apellido Materno minimo de 3 caracteres.',
            'Tipo_pago.required' => 'Es necesario una opcion de forma de pago.',
            'disciplina_id.required' => 'Es necesario una Disciplina.'
        ];
        $rules = [
            'Nombres' => 'required|min:3',
            'Apellido_Paterno' => 'required|min:3',
            'Apellido_Materno' => 'required|min:3',
            'Tipo_pago' => 'required',
            'disciplina_id' => 'required'
        ];

        $this->validate($request, $rules, $messages);
        
        $instructor->update($request->all());

        return redirect('/admin/Instructor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instructor  $instructor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instructor $instructor)
    {
        $instructor->delete();
        return back();
    }
    public function export($dateInit, $dateEnd, $idInstructor)
    {
        $dateIni = $dateInit;
        $dateFin = $dateEnd;
        
        return (new InstructorExport($dateIni, $dateFin, $idInstructor))->download('instructores.xlsx');
    }
}
