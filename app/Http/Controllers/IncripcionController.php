<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Incripcion;
use App\Disciplina;
use App\Pago;
use App\Deuda;
use Carbon\Carbon;
use PDF;
use App\Exports\IncripcionsExport;

class IncripcionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        if($search)
        {
            $query = '%' . $search . '%';            
            $incripcions = Cliente::where('Nombre', 'like', $query)->orderBy('id')->paginate(10);            
            //$incripcions = Cliente::where('Nombres', 'like', $query)->getClientes();    
            //$incripcions = Incripcion::where('Nombres', 'like', $query)->obtenerClientes();    
        }else{
            $incripcions = Cliente::orderBy('id', 'desc')->paginate(10);
            //$incripcions = Cliente::getClientes();
            //$incripcions = Incripcion::obtenerClientes();
        }                
        return view('admin.incripcion.index', compact('incripcions', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pagos = Pago::all();
        $disciplinas = Disciplina::all();
        return view('admin.incripcion.create', compact('pagos', 'disciplinas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'nro_identificacion.required' => 'Es necesario cargar carnet de identidad.',
            'nro_identificacion.min' => 'Nro de identificacion es necesario un mínimo de 5 dígitos',
            'Nombre.required' => 'Es necesario cargar Nombre al Cliente.',
            'Nombre.min' => 'Nombre del cliente como minimo 5 caracteres.',
            'Apellido_Paterno.required' => 'Es necesario un apellido Paterno para el cliente.',
            'Apellido_Materno.required' => 'Es necesario un apellido Materno para el cliente.'            ,
            'disciplina_id.required' => 'Es necesario cargar una disciplina.',
            'pago_id.required' => 'Favor escoja un método de pago'
        ];
        $rules = [
            'nro_identificacion' => 'required|min:3',
            'Nombre' => 'required|min:0',
            'Apellido_Paterno' => 'required',
            'Apellido_Materno' => 'required',
            'disciplina_id' => 'required',
            'pago_id' => 'required'
        ];
        $this->validate($request, $rules, $messages);


        $FechaInicio = ($request->Fecha_inicio === "__.__.____") ? null : Carbon::parse($request->Fecha_inicio)->format('Y-m-d');
        $FechaFin = ($request->Fecha_Fin === "__.__.____") ? null : Carbon::parse($request->Fecha_Fin)->format('Y-m-d');

        $cliente = new Cliente();
        $getId = $cliente->getCliente($request);        
        $incripcion = new Incripcion;
        $incripcion->cliente_id = $getId;   
        $incripcion->disciplina_id = $request->disciplina_id;
        $incripcion->pago_id = $request->pago_id;
        $incripcion->pago_total = $request->pago_total;
        $incripcion->fecha_inicio = $FechaInicio;
        $incripcion->fecha_fin = $FechaFin;        
        $incripcion->user_id = auth()->user()->id;
        $incripcion->save();

        return redirect('/admin/incripcion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $FechaInicio = $request->Fecha_inicio;
        $FechaFin = $request->Fecha_Fin;
        $montoAlumnos = 0;
        $montoTotal = 0;
        
        if($FechaInicio)
        {            
            $incripcions = Incripcion::join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')
                                       ->select('incripcions.*', 'clientes.*')
                                       ->whereBetween('incripcions.created_at', [$FechaInicio, $FechaFin])    
                                       ->orderBy('incripcions.id', 'desc')
                                       ->paginate(10);            

            $montoAlumnos = $incripcions->count();
            $montoTotal = $incripcions->sum('pago_total');
        }else
        {
            $incripcions = Incripcion::getIncripcionDay();            
        }

        

        return view('admin.incripcion.show', compact('incripcions', 'FechaInicio', 'FechaFin', 'montoAlumnos', 'montoTotal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Incripcion $incripcion)
    {                
        $pagos = Pago::all();
        $disciplinas = Disciplina::all();

        //Crear metodo que recupere el cliente de la inscripcion para relacionarlo. y despues crear una vista solo para clientes existentes.        
        //$incripcion = Incripcion::find($incripcion->id);
        $cliente = Cliente::find($incripcion->cliente_id);        
        return view('admin.incripcion.edit', compact('incripcion', 'pagos', 'disciplinas', 'cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incripcion $incripcion)
    {
        $messages = [
            'nro_identificacion.required' => 'Es necesario cargar carnet de identidad.',
            'nro_identificacion.min' => 'Nro de identificacion es necesario un mínimo de 5 dígitos',
            'Nombre.required' => 'Es necesario cargar Nombre al Cliente.',
            'Nombre.min' => 'Nombre del cliente como minimo 5 caracteres.',
            'Apellido_Paterno.required' => 'Es necesario un apellido Paterno para el cliente.',
            'Apellido_Materno.required' => 'Es necesario un apellido Materno para el cliente.'            ,
            'disciplina_id.required' => 'Es necesario cargar una disciplina.',
            'pago_id.required' => 'Favor escoja un método de pago'
        ];
        $rules = [
            'nro_identificacion' => 'required|min:2',
            'Nombre' => 'required|min:0',
            'Apellido_Paterno' => 'required',
            'Apellido_Materno' => 'required',
            'disciplina_id' => 'required',
            'pago_id' => 'required'
        ];
        $this->validate($request, $rules, $messages);


        $id = $incripcion->cliente_id;
        $cliente = Cliente::find($id);
        $cliente->nro_identificacion = $request->nro_identificacion;
        $cliente->Nombre = $request->Nombre;
        $cliente->Apellido_Paterno = $request->Apellido_Paterno;
        $cliente->Apellido_Materno = $request->Apellido_Materno;
        $cliente->Telefono = $request->Telefono;
        $cliente->save();
        $FechaInicio = ($request->Fecha_inicio === "__.__.____") ? null : Carbon::parse($request->Fecha_inicio)->format('Y-m-d');
        $FechaFin = ($request->Fecha_Fin === "__.__.____") ? null : Carbon::parse($request->Fecha_Fin)->format('Y-m-d');

        //$incripcion->update($request->only(['pago_total', 'pago_id', 'disciplina_id', $FechaInicio, $FechaFin]));
        
        $incripcion->pago_total = $request->pago_total;
        $incripcion->pago_id = $request->pago_id;
        $incripcion->disciplina_id = $request->disciplina_id;
        $incripcion->Fecha_inicio = $FechaInicio;
        $incripcion->Fecha_Fin = $FechaFin;       
        $incripcion->user_id = auth()->user()->id;
        $incripcion->save();
        

        return redirect()->route('cliente', ['id'=>$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function renovar($id)
    {
        $cliente = Cliente::find($id);
        $pagos = Pago::all();
        $disciplinas = Disciplina::all();
            
        return view('admin.incripcion.renovar', compact('cliente', 'pagos', 'disciplinas'));
    }
    public function renovarstore(Request $request)
    {        
        $id = $request->cliente_id;
        $FechaInicio = ($request->Fecha_inicio == "__.__.____") ? null : Carbon::parse($request->Fecha_inicio)->format('Y-m-d');
        $FechaFin = ($request->Fecha_Fin == "__.__.____") ? null : Carbon::parse($request->Fecha_Fin)->format('Y-m-d');    
        
        $messages = [
            'nro_identificacion.required' => 'Es necesario cargar carnet de identidad.',
            'nro_identificacion.min' => 'Nro de identificacion es necesario un mínimo de 5 dígitos',
            'Nombre.required' => 'Es necesario cargar Nombre al Cliente.',
            'Nombre.min' => 'Nombre del cliente como minimo 5 caracteres.',
            'Apellido_Paterno.required' => 'Es necesario un apellido Paterno para el cliente.',
            'Apellido_Materno.required' => 'Es necesario un apellido Materno para el cliente.',
            'disciplina_id.required' => 'Es necesario cargar una disciplina.',
            'pago_id.required' => 'Favor escoja un método de pago'
        ];
        $rules = [
            'nro_identificacion' => 'required|min:2',
            'Nombre' => 'required|min:0',
            'Apellido_Paterno' => 'required',
            'Apellido_Materno' => 'required',
            'disciplina_id' => 'required',
            'pago_id' => 'required'
        ];
        $this->validate($request, $rules, $messages);


        $request->merge([                
            'Fecha_inicio' => $FechaInicio,
            'Fecha_Fin' => $FechaFin,
            'user_id' => auth()->user()->id
        ]);
        Incripcion::create($request->only(['disciplina_id', 'cliente_id', 'pago_id', 'pago_total', 'Fecha_inicio', 'Fecha_Fin', 'user_id']));

        //$incripcion = new Incripcion();
        //$incripcion->cliente_id = $request->cliente_id;
        //$incripcion->disciplina_id = $request->disciplina_id;
        //$incripcion->pago_id = $request->pago_id;
        //$incripcion->pago_total = $request->pago_total;
        //$incripcion->Fecha_inicio = $FechaInicio;
        //$incripcion->Fecha_Fin = $FechaFin;
        //$incripcion->save();
        
        return redirect()->route('cliente', ['id'=>$id]);
    }
    public function reportcliente($cliente_id)
    {        
        $incripcions = Cliente::recoveryClient($cliente_id);        
        $cliente = Cliente::find($cliente_id);
        $deudas = Incripcion::getDeuda($cliente_id);   
                 
        return view('admin.incripcion.reportcliente', compact('incripcions', 'cliente_id', 'cliente', 'deudas'));
    }
    public function deudacliente(Request $request)
    {        
        $messages = [
            'monto_deuda.required' => 'Debe tener un monto.'
        ];
        $rules = [
            'monto_deuda' => 'required'
        ];
        $this->validate($request, $rules, $messages);

        Deuda::create($request->all());        
        return back();
    }
    public function imprimir($id)
    {
        // metodo antiguo para guardado ya que no se usa request para guardar el log de la factura
        $imprimir = Incripcion::getClientes($id);        
        $data = [
                 'nombre' => $imprimir->Nombre . ' ' . $imprimir->Apellido_Paterno . ' ' . $imprimir->Apellido_Materno,
                 'nro_carnet' => $imprimir->nro_identificacion,
                 'disciplina' => $imprimir->disciplina->Descripcion . ' - ' . $imprimir->disciplina->hora_inicio . ' - ' . $imprimir->disciplina->hora_fin,
                 'monto' => $imprimir->pago_total
                ];        
        $pdf  = PDF::loadView('admin.incripcion.imprimir', $data);        
        $pdf->set_base_path('/css/bootstrap.min.css');
        return $pdf->stream('prueba.pdf');
    }
    public function destroydeuda($id)
    {   
        
        $deuda = Deuda::find($id);                    
        $deuda->estado_deuda = "Pagado";        
        $deuda->save();        
        $incripcion = Incripcion::find($deuda->incripcion_id);             
        $incripcion->pago_total = $incripcion->pago_total + $deuda->monto_deuda;
        $incripcion->save();
        
        $deuda->delete();
        return back(); 
    }
    public function export($dateInit, $dateEnd)
    {        
        $dateIni = $dateInit;
        $dateFin = $dateEnd;
    
        //$dateIni = "2018-10-28";
        //$dateFin = "2018-10-31";
        return (new IncripcionsExport($dateIni, $dateFin))->download('inscripcion.xlsx');
    }
    public function imprimir2($id)
    {
        $imprimir = Incripcion::getClientes($id);      
        $fecha = Carbon::now()->format('d/m/Y');  
        return view('admin.incripcion.imprimir2', compact('imprimir', 'fecha'));
    }
    public function imprimir3($id)
    {
        $imprimir = Incripcion::getClientes($id);
        $deuda = Deuda::where('incripcion_id', '=', $imprimir->id)->first();
        $fecha = Carbon::now()->format('d/m/Y');  
        
        return view('admin.incripcion.imprimir3', compact('fecha', 'imprimir', 'deuda'));
    }
    public function imprimir4($id)
    {
        $imprimir = Incripcion::getClientes($id);
        $deuda = Deuda::where('incripcion_id', '=', $imprimir->id)->first();
        $fecha = Carbon::now()->format('d/m/Y');  
        
        return view('admin.incripcion.imprimir4', compact('fecha', 'imprimir', 'deuda'));
    }

}



