<?php

namespace App\Http\Controllers;

use App\DetalleVenta;
use App\Venta;
use Illuminate\Http\Request;

class DetalleVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'producto_id.required' => 'Favor cargar un producto.'            
        ];
        $rules = [
            'producto_id' => 'required',            
        ];
        $this->validate($request, $rules, $messages);

        $venta = new Venta();        
        $obtenerVentas = Venta::getVentaOld();    
        
        if(count($obtenerVentas) > 0)
        {          
            foreach($obtenerVentas as $obtenerVenta)  
            {
                $request->merge([
                    'venta_id' => $obtenerVenta->id,
                    'cantidad' => 1
                ]);
                DetalleVenta::create($request->all());
            }            
        }
        else{            
            $request->merge([
                'venta_id' => $venta->getVentas(),
                'cantidad' => 1
            ]);
            DetalleVenta::create($request->all());
        }
        
        //return redirect('/admin/ventas/index');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleVenta $detalleVenta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleVenta $detalleVenta)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $detalleVenta = DetalleVenta::find($request->detalleventa_id);                
        $detalleVenta->update($request->only('cantidad'));                
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detalleVenta = DetalleVenta::find($id);
        $detalleVenta->delete();
        return back();
    }
}
