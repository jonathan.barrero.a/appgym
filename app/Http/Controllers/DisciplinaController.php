<?php

namespace App\Http\Controllers;

use App\Disciplina;
use Illuminate\Http\Request;
use Carbon\Carbon;

class DisciplinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        if($search)
        {
            $query = '%' . $search . '%';
            $disciplinas = Disciplina::where('Descripcion', 'like', $query)->orderBy('id')->paginate(10);                
        }else{
            $disciplinas = Disciplina::orderBy('id')->paginate(10);
        }
        
        return view('admin.disciplina.index', compact('disciplinas', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return view('admin.disciplina.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Descripcion.required' => 'Es necesario cargar una descripcion.',
            'hora_inicio.required' => 'Es necesario cargar una hora de inicio.',
            'hora_fin.required' => 'Es necesario cargar una hora de fin.'
        ];
        $rules = [
            'Descripcion' => 'required|min:0',
            'hora_inicio' => 'required|min:0',    
            'hora_fin' => 'required|min:0'
        ];
        $this->validate($request, $rules, $messages);

        Disciplina::create($request->all());
        //$disciplina = new Disciplina();
        //$disciplina->Descripcion = $request->Descripcion;
        //$disciplina->hora_inicio = $request->hora_inicio;
        //$disciplina->hora_fin = $request->hora_fin;
        //$disciplina->save();

        return redirect('/admin/disciplina');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function show(Disciplina $disciplina)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function edit(Disciplina $disciplina)
    {        
        return view('admin.disciplina.edit', compact('disciplina'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Disciplina $disciplina)
    {                
        $disciplina->update($request->only('Descripcion', 'hora_inicio', 'hora_fin'));
        return redirect('/admin/disciplina');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Disciplina  $disciplina
     * @return \Illuminate\Http\Response
     */
    public function destroy(Disciplina $disciplina)
    {                  
        $disciplina->delete();                            
        return back();
    }
}
