<?php

namespace App\Http\Controllers;

use App\DetalleCompra;
use App\Compra;
use Illuminate\Http\Request;


class DetalleCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'producto_id.required' => 'Favor cargar un producto.'            
        ];
        $rules = [
            'producto_id' => 'required',            
        ];
        $this->validate($request, $rules, $messages);
        
        $compra = new Compra();        
        $obtenerCompras = Compra::getCompraOld();    
        
        if(count($obtenerCompras) > 0)
        {          
            foreach($obtenerCompras as $obtenerCompra)  
            {
                $request->merge([
                    'compra_id' => $obtenerCompra->id,
                    'cantidad' => 1
                ]);
                DetalleCompra::create($request->all());
            }            
        }
        else{            
            $request->merge([
                'compra_id' => $compra->getCompras(),
                'cantidad' => 1
            ]);
            DetalleCompra::create($request->all());
        }
        
        //return redirect('/admin/ventas/index');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function show(Compra $compra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function edit(Compra $compra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $detalleCompra = DetalleCompra::find($request->detallecompra_id);                
        $detalleCompra->update($request->only('cantidad'));                
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compra  $compra
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detalleCompra = DetalleCompra::find($id);
        $detalleCompra->delete();
        return back();
    }
}
