<?php

namespace App\Http\Controllers;

use App\Inventario;
use Illuminate\Http\Request;

class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        if($search)
        {
            $query = '%' . $search . '%';            
            $inventarios = Inventario::where('nombre', 'like', $query)->orderBy('id')->paginate(10);                        
        }else{
            $inventarios  = Inventario::orderBy('id', 'desc')->paginate(10);            
        }                    
        return view('admin.inventario.index', compact('inventarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.inventario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'nombre.required' => 'Es necesario Cargar un Nombre.',
            'nombre.min' => 'Campo Nombres necesita minimo de 3 caracteres.',
            'Descripcion.required' => 'Es necesario Apellido Paterno',
            'Descripcion.min' => 'Campo Apellido Paterno minimo de 3 caracteres.'
        ];
        $rules = [
            'nombre' => 'required|min:3',
            'Descripcion' => 'required|min:3'            
        ];

        $this->validate($request, $rules, $messages);
        Inventario::create($request->all());
        return redirect('/admin/inventario');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function show(Inventario $inventario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventario $inventario)
    {
        return view('admin.inventario.edit', compact('inventario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventario $inventario)
    {
        $messages = [
            'nombre.required' => 'Es necesario Cargar un Nombre.',
            'nombre.min' => 'Campo Nombres necesita minimo de 3 caracteres.',
            'Descripcion.required' => 'Es necesario Apellido Paterno',
            'Descripcion.min' => 'Campo Apellido Paterno minimo de 3 caracteres.'
        ];
        $rules = [
            'nombre' => 'required|min:3',
            'Descripcion' => 'required|min:3'            
        ];

        $this->validate($request, $rules, $messages);
        
        $inventario->update($request->all());
        return redirect('/admin/inventario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventario  $inventario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventario $inventario)
    {
        $inventario->delete();
        return back();
    }
}
