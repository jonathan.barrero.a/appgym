<?php

namespace App\Http\Controllers;

use App\Venta;
use Illuminate\Http\Request;
use App\DetalleVenta;
use App\Producto;
use Carbon\Carbon;
use App\Exports\VentasExport;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $ModelDetalleVenta = new DetalleVenta();
        $productos = Producto::get();
        $detalleventas = DetalleVenta::getDetalle();                      
        //$detalleventas = Venta::getDetalleVenta();        
        $objetoVentas = Venta::where('estado', '=', 'Pendiente')->select('id')->first();        

        ($objetoVentas) ? $idVentas = $objetoVentas->id : $idVentas = 0;          
        $total = $ModelDetalleVenta->getTotal($detalleventas);

        return view('admin.ventas.index', compact('detalleventas', 'productos', 'idVentas', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idVenta = $request->venta_id;
        $venta = Venta::find($idVenta);        
        $venta->estado = "Pagado";
        $venta->save();
        $stocks = DetalleVenta::where('venta_id', '=', $idVenta)->get();
        foreach($stocks as $stock)
        {
            $producto = Producto::find($stock->producto_id);            
            $producto->stock = $producto->stock - $stock->cantidad;
            $producto->save();
        }
        $notification = 'La venta fue realizada.';
    	return back()->with(compact('notification'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {        
        $FechaInicio = $request->Fecha_inicio;
        $FechaFin = $request->Fecha_Fin;
        //$FechaInicio= "2018-10-29";
        //$FechaFin = "2018-10-30";
          
        //($search == '__.__.____') ? null : Carbon::parse($search)->format('Y-m-d');        
        
        if($FechaInicio)
        {            

            $detalleventas = DetalleVenta::join('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                                          ->where('ventas.estado', '=', 'Pagado')
                                          ->select('detalle_ventas.*', 'ventas.user_id')
                                          ->whereBetween('ventas.created_at', [$FechaInicio, $FechaFin])    
                                          ->orderBy('detalle_ventas.id', 'desc')
                                          ->get();                                    
        }else{                        
            $detalleventas = DetalleVenta::getDetalleVentas();                     
        }
        return view('admin.ventas.show', compact('detalleventas', 'FechaInicio', 'FechaFin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function edit(Venta $venta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Venta $venta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Venta $venta)
    {
        
    }
    public function export($dateInit, $dateEnd)
    {
        $dateIni = $dateInit;
        $dateFin = $dateEnd;

        return (new VentasExport($dateIni, $dateFin))->download('ventas.xlsx');
    }
}
