<?php

namespace App\Http\Controllers;

use App\Unidades;
use Illuminate\Http\Request;

class UnidadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        if($search)
        {
            $query = '%' . $search . '%';
            $unidades = Unidades::where('Descripcion', 'like', $query)->orderBy('id')->paginate(10);
        }else{
            $unidades = Unidades::orderBy('id')->paginate(10);
        }
        
        return view('admin.unidades.index', compact('unidades', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unidades = Unidades::all();        

        return view('admin.unidades.create', compact('unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'Descripcion.required' => 'Es necesario Cargar un Tipo de Unidad.',
            'Descripcion.min' => 'Es necesario un minimo de 3 caracteres.'
        ];
        $rules = [
            'Descripcion' => 'required|min:3'
        ];

        $this->validate($request, $rules, $messages);

        Unidades::create($request->all());

        return redirect('/admin/unidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function show(Unidades $unidades)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function edit(Unidades $unidades)
    {
        return view('admin.unidades.edit', compact('unidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unidades $unidades)
    {
        $unidades->update($request->only('Descripcion'));
        
        return redirect('/admin/unidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unidades  $unidades
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unidades $unidades)
    {
        $unidades->delete();
        return back();
    }
}
