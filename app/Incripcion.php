<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Incripcion extends Model
{
    use SoftDeletes;

    protected $fillable = ['cliente_id', 'pago_total', 'pago_id', 'disciplina_id', 'Fecha_inicio', 'Fecha_Fin', 'Estado_incripcion', 'user_id'];

    public function cliente()
    {
        return $this->belongsTo(Cliente::class)->withTrashed();
    }
    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class)->withTrashed();
    }
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
    public function deuda()
    {
        return $this->belongsTo(Deuda::class)->withTrashed();
    }
    public function scopeObtenerClientes($query)
    {
        return $query->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')                     
                     ->paginate(10);
    }
    public function scopeGetClientes($query, $idincripcion)
    {
        return $query->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')                                          
                     ->where('incripcions.id', '=', $idincripcion)
                     ->first();
    }
    public function getDayVencido($id)
    {        
        $this->find($id);
        $this->Estado_incripcion = 'Vencido';
        $this->save();
        return $this->Estado_incripcion;
    }
    public function scopeGetVencidos($query)
    {
        return $query->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')                     
                     ->where('Fecha_Fin', '<=', \Carbon\Carbon::now())
                     ->count();
    }
    public function scopeGetDeuda($query, $id)
    {
        return $query->join('deudas', 'incripcions.id', '=', 'deudas.incripcion_id')                                                               
                     ->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')
                     ->select('incripcions.*', 'deudas.*')
                     ->where('clientes.id', '=', $id)
                     ->get();
    }
    public function scopeGetActualizarVencido($query)
    {
        return $query->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')                     
                     ->where('Fecha_Fin', '<=', \Carbon\Carbon::now())
                     ->get();
    }
    public function scopeGetIncripcionDay($query)
    {
        return $query->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')
                     ->select('incripcions.*', 'clientes.*')
                     ->where('incripcions.Fecha_inicio', \Carbon\Carbon::now()->format('Y-m-d'))
                     ->orderBy('incripcions.id', 'desc')
                     ->paginate(10);
    }

}
