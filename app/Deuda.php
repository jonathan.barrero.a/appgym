<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deuda extends Model
{    
    use SoftDeletes;

    protected $fillable = ['incripcion_id', 'monto_deuda', 'estado_deuda'];

    public function incripcion()
    {
        return $this->belongsTo(Incripcion::class)->withTrashed();
    }
}
