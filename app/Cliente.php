<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;

    public function scopeClienteEstado()
    {
        
    }    
    public function cliente()
    {
        return $this->hasMany(Incripcion::class)->withTrashed();
    }
    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class)->withTrashed();
    }
    public function getCliente($request)
    {        
        $this->nro_identificacion = $request->nro_identificacion;
        $this->Nombre = $request->Nombre;
        $this->Apellido_Paterno = $request->Apellido_Paterno;
        $this->Apellido_Materno = $request->Apellido_Materno;
        $this->Telefono = $request->Telefono;
        $this->Direccion = $request->Direccion;
        $this->Email = $request->Email;        
        $this->save();        
        return $this->id;
    }
    public function scopeGetClientes($query)
    {
        return $query->join('incripcions', 'clientes.id', '=', 'incripcions.cliente_id')                     
                     ->orderBy('incripcions.cliente_id', 'desc')
                     ->paginate(10);
    }
    public function scopeRecoveryClient($query, $id)
    {        
        return $query->join('incripcions', 'clientes.id', '=', 'incripcions.cliente_id')
                     ->where('incripcions.cliente_id', '=', $id)     
                     ->orderBy('incripcions.id', 'desc')                
                     ->paginate(10);
    }   
    public function scopeRecoveryClientEdit($query, $id)
    {        
        return $query->join('incripcions', 'clientes.id', '=', 'incripcions.cliente_id')
                     ->where('incripcions.cliente_id', '=', $id);
    }    
}
