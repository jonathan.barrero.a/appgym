<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;

    protected $fillable = ['nombre', 'codigo_producto', 'precio_costo', 'precio_venta', 'stock', 'stock_min', 'categoria_id', 'unidades_id', 'user_id'];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }
    public function unidades()
    {
        return $this->belongsTo(Unidades::class);
    }
}
