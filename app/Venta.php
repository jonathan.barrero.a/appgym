<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{    

    protected $fillable = ['user_id', 'estado'];
    public function detalleventas()
    {
        return $this->hasMany(DetalleVenta::class);
    }    
    public function usuario()
    {
        return $this->hasOne(User::class);
    }
    public function getVentas()
    {     
        $this->user_id = auth()->user()->id;
        $this->save();  
        //$this->create([
        //    'user_id' => auth()->user()->id
        //]);
                
        return $this->id;
    }
    public function scopeGetVentaOld($query)
    {
        return $query->where('ventas.estado', '=', 'Pendiente')
                     ->get();                     
    }
    public function scopeGetDetalleVenta($query)
    {
        return $query->join('detalle_ventas', 'ventas.id', '=', 'detalle_ventas.venta_id')
                     ->where('ventas.estado', '=', 'Pendiente')
                     ->get();                     
    }
    public function scopeGetTotalVentas($query)
    {
        return $query->join('detalle_ventas', 'ventas.id', '=', 'detalle_ventas.venta_id')
                     ->where('ventas.estado', '=', 'Pendiente')
                     ->get();                     
    }
}
