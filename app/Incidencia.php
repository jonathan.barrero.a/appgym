<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
    protected $fillable = ['instructor_id', 'fecha_incidencia', 'descripcion'];
}
