<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Instructor extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['Nombres', 'Apellido_Paterno', 'Apellido_Materno', 'Direccion', 'Telefono', 'Tipo_pago', 'porcentaje', 'disciplina_id'];


    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class);
    }
}
