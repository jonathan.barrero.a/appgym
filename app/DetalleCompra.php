<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    protected $fillable = ['compra_id', 'producto_id', 'cantidad'];

    public function compras()
    {
        return $this->belongsTo(Compra::class);
    }
    public function producto()
    {
        return $this->belongsTo(Producto::class)->withTrashed();
    }
    public function getTotalAttibute()
    {
        $total = 0;
        foreach($this->detallecompras as $detallecompra)
        {
            $total += $detalle->cantidad * $detalle->producto->precio_venta;            
        }
        return $total;
    }
    public function getTotal($detallecompras)
    {
        $total = 0;
        
        foreach($detallecompras as $detallecompra)
        {            
            $total += $detallecompra->cantidad * $detallecompra->producto->precio_compra;            
        }
        return $total;
    }
    public function scopeGetDetalle($query)
    {
        return $query->leftjoin('compras', 'detalle_compras.compra_id', '=', 'compras.id')
                     ->where('compras.estado', '=', 'Pendiente')
                     ->select('detalle_compras.*')
                     ->get();                     
    }
    public function scopeGetDetalleCompras($query)
    {
        return $query->leftjoin('compras', 'detalle_compras.compra_id', '=', 'compras.id')
                     ->where('compras.estado', '=', 'Pagado')
                     ->select('detalle_compras.*', 'compras.user_id')
                     ->orderBy('detalle_compras.id', 'desc')
                     ->get();                     
    }
}
