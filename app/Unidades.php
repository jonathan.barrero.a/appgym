<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidades extends Model
{
    use SoftDeletes;

    protected $fillable = ['Descripcion'];
}
