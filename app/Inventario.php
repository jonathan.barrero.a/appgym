<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventario extends Model
{
    use SoftDeletes;

    protected $fillable = ['nombre', 'Descripcion', 'codigo_producto', 'precio_costo', 'stock'];
}
