<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $fillable = ['user_id', 'estado'];

    public function detallecompras()
    {
        return $this->hasMany(Compra::class);
    }
    public function usuario()
    {
        return $this->hasOne(User::class);
    }
    public function scopeGetCompraOld($query)
    {
        return $query->where('compras.estado', '=', 'Pendiente')
                     ->get();                     
    }
    public function getCompras()
    {     
        $this->user_id = auth()->user()->id;
        $this->save();  
        //$this->create([
        //    'user_id' => auth()->user()->id
        //]);
                
        return $this->id;
    }
}
