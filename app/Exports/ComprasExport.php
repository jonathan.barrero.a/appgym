<?php

namespace App\Exports;

use App\Compra;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ComprasExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct(string $dateIni, string $dateFin)
    {        
        $this->FechaIni = $dateIni;
        $this->FechaFin = $dateFin;        
    }

    public function query()
    {
        return Compra::query()->join('detalle_compras', 'detalle_compras.compra_id', '=', 'compras.id') 
                                  ->join('productos', 'detalle_compras.producto_id', '=', 'productos.id')                               
                                  ->select('compras.id', 'productos.nombre', 'productos.codigo_producto', 'productos.stock', 'productos.precio_costo', 'detalle_compras.cantidad')
                                  ->whereBetween('detalle_compras.created_at', [ $this->FechaIni, $this->FechaFin]);
    }
    public function headings(): array
    {
        return [
            '#',
            'Nombre del producto',
            'Codigo del producto',
            'Cantidad Stock',
            'Precio Costo',
            'Cantidad de compra',
        ];
    }
}
