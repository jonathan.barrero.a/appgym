<?php

namespace App\Exports;

use App\Incripcion;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class IncripcionsExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct(string $dateIni, string $dateFin)
    {        
        $this->FechaIni = $dateIni;
        $this->FechaFin = $dateFin;        
    }

    public function query()
    {
        return Incripcion::query()->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')
                                  ->join('disciplinas', 'incripcions.disciplina_id', '=', 'disciplinas.id')
                                  ->select('clientes.Nombre', 'clientes.Apellido_Paterno', 'clientes.nro_identificacion', 'disciplinas.Descripcion', 'disciplinas.hora_inicio', 'disciplinas.hora_fin', 'incripcions.pago_total', 'incripcions.pago_total')
                                  ->whereBetween('incripcions.created_at', [ $this->FechaIni, $this->FechaFin]);
    }
    public function headings(): array
    {
        return [
            '#',
            'Nombre del Cliente',
            'Apellido Cliente',
            'Nro de identificacion',
            'Disciplina',
            'Hora Inicio',
            'Hora Fin',
            'Pago total',
        ];
    }

}
