<?php

namespace App\Exports;

use App\Instructor;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class InstructorExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct(string $dateIni, string $dateFin, int $idInstructor)
    {        
        $this->FechaIni = $dateIni;
        $this->FechaFin = $dateFin;        
        $this->id = $idInstructor;
        
    }

    public function query()
    {
        return Instructor::query()->join('disciplinas', 'instructors.disciplina_id', '=', 'disciplinas.id')
                                  ->join('incripcions', 'disciplinas.id', '=', 'incripcions.disciplina_id')
                                  ->join('clientes', 'incripcions.cliente_id', '=', 'clientes.id')
                                  ->select('clientes.Nombre', 'clientes.Apellido_Paterno', 'clientes.nro_identificacion', 'disciplinas.Descripcion', 'disciplinas.hora_inicio', 'disciplinas.hora_fin', 'instructors.Nombres', 'instructors.porcentaje', 'incripcions.pago_total')
                                  ->where('instructors.id', '=', $this->id)    
                                  ->whereBetween('incripcions.created_at', [$this->FechaIni, $this->FechaFin]);
                                
    }
    public function headings(): array
    {
        return [
            'Nombre del cliente',
            'Apellido del cliente',
            'Nro de Carnet',
            'Disciplina',
            'Hora inicio',
            'Hora fin',
            'Nombre del Instructor',            
            'Porcentaje del Instructor',
            'Pago Total Alumno',            
        ];
    }
}
