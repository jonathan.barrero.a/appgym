<?php

namespace App\Exports;

use App\Venta;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;


class VentasExport implements FromQuery, WithHeadings
{
    use Exportable;


    public function __construct(string $dateIni, string $dateFin)
    {        
        $this->FechaIni = $dateIni;
        $this->FechaFin = $dateFin;        
    }


    public function query()
    {
        return Venta::query()->join('detalle_ventas', 'detalle_ventas.venta_id', '=', 'ventas.id') 
                                  ->join('productos', 'detalle_ventas.producto_id', '=', 'productos.id')                               
                                  ->select('ventas.id', 'productos.nombre', 'productos.codigo_producto', 'productos.stock', 'productos.stock_min', 'productos.precio_venta', 'detalle_ventas.cantidad')
                                  ->whereBetween('detalle_ventas.created_at', [ $this->FechaIni, $this->FechaFin]);
    }
    public function headings(): array
    {
        return [
            '#',
            'Nombre Producto',
            'Codigo del producto',
            'Stock',
            'Stock Minimo',
            'Precio de venta',
            'Cantidad',            
        ];
    }
}
