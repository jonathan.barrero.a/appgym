<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{    
    //No requiere borraro logico merece borrado fisico
    protected $fillable = ['venta_id', 'producto_id', 'cantidad'];

    public function ventas()
    {
        return $this->belongsTo(Venta::class);
    }
    public function producto()
    {
        return $this->belongsTo(Producto::class)->withTrashed();
    }
    public function getTotalAttibute()
    {
        $total = 0;
        foreach($this->detalleventas as $detalleventa)
        {
            $total += $detalle->cantidad * $detalle->producto->precio_venta;            
        }
        return $total;
    }
    public function getTotal($detalleventas)
    {
        $total = 0;
        
        foreach($detalleventas as $detalleventa)
        {            
            $total += $detalleventa->cantidad * $detalleventa->producto->precio_venta;            
        }
        return $total;
    }
    public function scopeGetDetalle($query)
    {
        return $query->leftjoin('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                     ->where('ventas.estado', '=', 'Pendiente')
                     ->select('detalle_ventas.*')
                     ->get();                     
    }
    public function scopeGetDetalleVentas($query)
    {
        return $query->leftjoin('ventas', 'detalle_ventas.venta_id', '=', 'ventas.id')
                     ->where('ventas.estado', '=', 'Pagado')
                     ->select('detalle_ventas.*', 'ventas.user_id')
                     ->orderBy('detalle_ventas.id', 'desc')
                     ->get();                     
    }
}
