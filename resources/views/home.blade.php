<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Gym</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="/css/demo.css" rel="stylesheet" />

    <!-- Estilos de choise JS para select -->
    <link href="/css/choise.min.css" rel="stylesheet" />

    <!-- Estilo para el data picker fechas -->
    <link href="/css/datepicker.css" rel="stylesheet" />

    <!--  Fonts and icons     -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!-- <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'> -->
    <link href="/css/themify-icons.css" rel="stylesheet">


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    

Usage

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="black" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo text-center">
                <img src="{{ url('/img/logogym.png') }}" alt="Thumbnail Image" class="img-raised img-circle" style="width:60px; height:60px;"> 
            </div>
            
            <ul class="nav">                
                <li>
                    <a class="collapsed" data-toggle="collapse" href="#formAdministration" aria-expanded="false">
                        <i class="ti-package"></i>
                        <p>Administracion</p>
                    </a>
                    <div id="formAdministration" class="collapse" aria-expanded="false" style="height: 0px;">
                        <ul class="nav">
                            <li class="{{ active('admin/incripcion') }}">
                                <a href="{{ url('/admin/incripcion') }}">                                    
                                    <i class="fa fa-users"></i><p>Inscripciones</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/Pagos') }}">
                                <a href="{{ url('/admin/Pagos') }}">                                    
                                <i class="fa fa-credit-card"></i><p>Tipos Pagos</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/usuario') }}">
                                <a href="{{ url('/admin/usuario') }}">                                
                                    <i class="fa fa-user"></i><p>Usuarios</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/disciplina') }}">
                                <a href="{{ url('/admin/disciplina') }}">                                    
                                    <i class="fa fa-address-book"></i><p>Disciplinas</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/Instructor') }}">
                                <a href="{{ url('/admin/Instructor') }}">                                    
                                    <i class="fa fa-align-left"></i><p>Instructores</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                                
                <li>
                    <a class="collapsed" data-toggle="collapse" href="#formProductos" aria-expanded="false">
                        <i class="ti-archive"></i>
                        <p>Lista Productos</p>
                    </a>
                    <div id="formProductos" class="collapse" aria-expanded="false" style="height: 0px;">
                        <ul class="nav">
                            <li class="{{ active('admin/unidades') }}">
                                <a href="{{ url('/admin/unidades') }}">                                    
                                    <i class="fa fa-bookmark"></i><p>Unidades</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/producto') }}">
                                <a href="{{ url('/admin/producto') }}">                                    
                                    <i class="fa fa-tasks"></i><p>Productos</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/categoria') }}">
                                <a href="{{ url('/admin/categoria') }}">                                
                                    <i class="fa fa-bars"></i><p>Categorias</p>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>

                <li class="{{ active('admin/ventas') }}">
                    <a href="{{ url('/admin/ventas') }}">
                        <i class="ti-text"></i>
                        <p>Ventas</p>
                    </a>
                </li>
                <li class="{{ active('admin/compras') }}">
                    <a href="{{ url('/admin/compras') }}">
                        <i class="far fa-money-bill-alt"></i>
                        <p>Compras</p>
                    </a>
                </li>                
                <li class="{{ active('admin/inventario') }}">
                    <a href="{{ url('/admin/inventario')}}">
                        <i class="fas fa-boxes"></i>
                        <p>Inventario</p>
                    </a>
                </li>   

                <li>
                    <a class="collapsed" data-toggle="collapse" href="#formReportes" aria-expanded="false">
                        <i class="ti-package"></i>
                        <p>Reportes</p>
                    </a>
                    <div id="formReportes" class="collapse" aria-expanded="false" style="height: 0px;">
                        <ul class="nav">
                            <li class="{{ active('admin/ventas/show') }}">
                                <a href="{{ url('/admin/ventas/show') }}">                                    
                                    <p>Reportes de ventas</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/compras/show') }}">
                                <a href="{{ url('/admin/compras/show') }}">                                    
                                    <p>Reportes de compras</p>
                                </a>
                            </li>
                            <li class="{{ active('/admin/Panel') }}">
                                <a href="{{ url('/admin/Panel') }}">                                    
                                    <p>Vencidos del Día</p>
                                </a>
                            </li>
                            <li class="{{ active('admin/incripcion/show/day') }}">
                                <a href="{{ url('/admin/incripcion/show/day') }}">                                    
                                    <p>Reporte Diario</p>
                                </a>
                            </li>
                            <!--<li class="{{ active('Pagos') }}">
                                <a href="{{ url('/admin/Pagos') }}">                                    
                                    <p>Reporte Mensual</p>
                                </a>
                            </li>                            -->
                            <li class="{{ active('admin/Instructor/show/day') }}">
                                <a href="{{ url('/admin/Instructor/show/day') }}">                                    
                                    <p>Reporte Instructores</p>
                                </a>
                            </li>                            
                        </ul>
                    </div>
                </li>


            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">                    
                    <a class="navbar-brand" href="#">Panel Administrativo</a>
                </div>
                <div class="dropdown">
                    <ul class="nav navbar-nav navbar-right">
                        @guest                            
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">Registro</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>                                    
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    @if(auth()->user()->is_admin)
                                        <a href="#" class="dropdown-item">Ubicaciones</a>
                                        <a href="#" class="dropdown-item">Item</a>
                                    @endif                                    
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar Sesion
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>

                </div>    
            </div>
        </nav>


        <div class="content">
            <div class="container">
                                
                @yield('content')                
                
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>        
                        <li>
                            <a href="#">
                                Licenses
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with by Jr-Codex
                </div>
            </div>
        </footer>

    </div>
</div>

@yield('modal')

</body>

    <!--   Core JS Files   -->
    <script src="/js/jquery.min.js" type="text/javascript"></script>
	<script src="/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="/js/bootstrap-checkbox-radio.js"></script>

	<!--  Charts Plugin -->
	<script src="/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	<script src="/js/paper-dashboard.js"></script>

	<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
	<script src="/js/demo.js"></script>

    <!-- Mascara de vanilla javascript -->
    <script src="/js/imask.js"></script>

    <!-- Jquery para datepicker -->
    <script src="/js/bootstrap-datepicker.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    @yield('scripts')
	
</html>