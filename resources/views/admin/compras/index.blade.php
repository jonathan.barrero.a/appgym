@extends('home')

@section('content')

    @if(session('notification'))
    <div class="row">
        <div class="alert alert-success">
            {{ session('notification') }}            
        </div>            
    </div>
    @endif

    <div class="row">
        <form action="{{ url('/admin/detallecompras') }}" method="post">
        {{ csrf_field() }}
            <div class="col-md-3">           
                <select name="producto_id" class="form-control border-input">
                        <option disabled selected>Seleccione un Producto</option>
                        @foreach($productos as $producto)                                            
                        <option value="{{ $producto->id }}">{{ $producto->nombre }}</option>
                        @endforeach
                </select>            
            </div>
            <div class="col-md-3">                        
                <button type="submit" class="btn btn-success"><i></i>Agregar Producto.</button>            
            </div>
        </form>
    </div>
    <div class="row" style="padding-top:50px;">
        <div class="card">
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>Nro. Producto</th>
                        <th>Nombre del producto</th>
                        <th>stock</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Sub Total</th>
                        <th class="col-md-2">Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($detallecompras as $detallecompra)
                        <tr>
                            <th>{{ $detallecompra->id }}</th>
                            <th>{{ $detallecompra->producto->nombre }}</th>
                            <th>{{ $detallecompra->producto->stock }}</th>
                            <th>{{ $detallecompra->producto->precio_costo }}</th>
                            <th>{{ $detallecompra->cantidad }}</th>
                            <th>{{ $detallecompra->cantidad * $detallecompra->producto->precio_costo  }}</th>
                            <th>
                                <form action="{{ url('/admin/detallecompras/'.$detallecompra->id) }}" method="post">                            
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}                                    
                                    <button id="btnEdit" type="button" class="btn btn-primary btn-simple btn-lg" data-toggle="modal" data-target="#compraedit" value="{{ $detallecompra->id }}" onclick="valuecantidad({{ $detallecompra->id }}, {{ $detallecompra->cantidad }})"><i class="fa fa-edit"></i></button>                                    
                                    <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>                                    
                                </form>
                            </th>
                        </tr>
                        @endforeach                        
                    </tbody>
                </table>
                <hr>
                <div class="row text-center">
                    <p><strong>Importa Total:</strong>{{ $total }}</p>
                </div>                
            </div>

            <div class="row text-center" style="padding-top:20px; padding-bottom:20px;">
                <form action="{{ url('/admin/compras') }}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="compra_id" value="{{ $idCompras }}">
                    <button type="submit" class="btn btn-success"><i class="fa fa-clipboard-check"></i>Confirmar Compra</button>
                </form>
            </div>            
        </div>
    </div>

@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="compraedit" tabindex="-1" role="dialog" aria-labelledby="EditModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="EditModalLabel">Agregar cantidada al Producto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">                    
                        <div class="content">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ url('/admin/detallecompras/edit') }}" method="post">
                                {{ csrf_field() }}                                                              
                                        <input id="hiddenedit" type="hidden" name="detallecompra_id" value="" />                                                             
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">                                            
                                                <input id="cantidadid" type="number" placeholder="Registrar nro Producto" class="form-control border-input" name="cantidad" value="">
                                            </div> 
                                        </div>                                                                
                                    <button class="btn btn-success">Agregar cantidad</button>                                                            
                            </form>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function valuecantidad(id, cantidad)
        {
            document.getElementById('hiddenedit').value = id;
            document.getElementById('cantidadid').value = cantidad;
        }
    </script>
@endsection