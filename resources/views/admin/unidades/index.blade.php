@extends('home')

@section('content')
    <div class="row text-center">
            <a href="{{ url('/admin/unidades/create') }}" class="btn btn-primary">Agregar Nueva Unidades</a>
    </div>

    <div class="row" style="padding-top:30px;">
        <form action="{{ url('/admin/unidades' )}}" method="get">
            <div class="form-group">
              <input type="text" name="search" class="form-control" placeholder="Buscar Categoria.">
            </div>
        </form>
    </div>

    <div class="row">
        <div class="card">        
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Descripcion Unidades</th>
                        <th class="col-md-2">Acciones</th>
                    </thead>
                    <tbody>
                        @foreach ($unidades as $unidad)
                        <tr>
                            <td>{{ $unidad->id }}</td>
                            <td>{{ $unidad->Descripcion }}</td>
                            <td>
                                <form method="post" action="{{ url('/admin/unidades/'.$unidad->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/unidades/'.$unidad->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection