@extends('printer')

@section('imprimir')
<div id="idPrint">
    <div class="row">
      <div class="col-md-4">
        <img src="{{ url('/img/logogym.png') }}" alt="Thumbnail Image" class="img-raised img-circle" style="width:70px; height:70px;"> 
      </div>
      <div class="col-md-4">
        <h2 id="idH">RECIBO</h2>
      </div>
      <div class="col-md-4">
        <h4>FECHA: {{ $fecha }} </h4> 
      </div>
    </div>
    <div class="row" style="padding-top:80px">
        <div class="col-md-6">
            <label class="control-label">Nombre:   </label>           
            {{ $imprimir->Nombre . ' ' . $imprimir->Apellido_Paterno . ' ' . $imprimir->Apellido_Materno }} 
        </div>
        <div class="col-md-6">
            <label class="control-label">Carnet:  </label>
            {{ $imprimir->nro_identificacion }}
        </div>        
    </div>
    <div class="row" style="padding-top:40px">
        <div class="col-md-6">
            <label class="control-label">Descripcion: <span>Pago de Mensualidad</span></label>
        </div>
        <div class="col-md-6">
            <label class="control-label">Disciplina:</label>
            {{ $imprimir->disciplina->Descripcion . ' ' . $imprimir->disciplina->hora_inicio . ' ' . $imprimir->disciplina->hora_fin }}
        </div>
    </div>
    <div class="row" style="padding-top:40px">
        <div class="col-md-4">
            <label class="control-label">Monto:  </label>
            {{ $imprimir->pago_total }}
        </div>
        <div class="col-md-4">
            <label class="control-label">Total:  </label>
            {{ $imprimir->pago_total }}
        </div>
    </div>
    <div class="row text-center" style="padding-top:40px">
        <label class="control-label">Firma:  </label>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
        <img src="{{ url('/img/logogym.png') }}" alt="Thumbnail Image" class="img-raised img-circle" style="width:70px; height:70px;"> 
      </div>
      <div class="col-md-4">
        <h2 id="idH">RECIBO</h2>
      </div>
      <div class="col-md-4">
        <h4>FECHA: {{ $fecha }} </h4> 
      </div>
    </div>
    <div class="row" style="padding-top:80px">
        <div class="col-md-6">
            <label class="control-label">Nombre:   </label>           
            {{ $imprimir->Nombre . ' ' . $imprimir->Apellido_Paterno . ' ' . $imprimir->Apellido_Materno }} 
        </div>
        <div class="col-md-6">
            <label class="control-label">Carnet:  </label>
            {{ $imprimir->nro_identificacion }}
        </div>        
    </div>
    <div class="row" style="padding-top:40px">
        <div class="col-md-6">
            <label class="control-label">Descripcion: <span>Pago de Mensualidad</span></label>
        </div>
        <div class="col-md-6">
            <label class="control-label">Disciplina:</label>
            {{ $imprimir->disciplina->Descripcion . ' ' . $imprimir->disciplina->hora_inicio . ' ' . $imprimir->disciplina->hora_fin }}
        </div>
    </div>
    <div class="row" style="padding-top:40px">
        <div class="col-md-4">
            <label class="control-label">Monto:  </label>
            {{ $imprimir->pago_total }}
        </div>
        <div class="col-md-4">
            <label class="control-label">Total:  </label>
            {{ $imprimir->pago_total }}
        </div>
    </div>
    <div class="row text-center" style="padding-top:40px">
        <label class="control-label">Firma:  </label>
    </div>
    <div class="row">
        <button type="button" class="btn btn-info" onclick="imprimirSelect('idPrint')">Imprimir</button>
    </div>
</div>
@endsection

@section('scripts')

    <script>
        function imprimirSelect(idPrint)
        {            
            var ContenidoImprimir = document.getElementById(idPrint).innerHTML;            
            var ContenidoOriginal = document.body.innerHTML;

            document.body.innerHTML = ContenidoImprimir;

            window.print();

            document.body.innerHTML = ContenidoOriginal;
        }
    </script>

@endsection