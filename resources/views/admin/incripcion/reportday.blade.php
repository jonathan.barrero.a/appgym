@extends('home')

@section('content')
    <div class="row">        
        <div class="row" style="padding-top:30px;">
            <form action="{{ url('/admin/incripcion' )}}" method="get">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Buscar Cliente.">
                </div>
            </form>
        </div>


        <div class="content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>                    
                    <th>Numero Identificacion</th>
                    <th>Nombre</th>                    
                    <th>Apellido</th>
                    <th>Telefono</th>
                    <th>Disciplina</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Fin</th>
                    <th>pago</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach($incripcions as $incripcion)
                    <tr>                        
                        <td>{{ $incripcion->nro_identificacion }}</td>
                        <td>{{ $incripcion->Nombre }}</td>
                        <td>{{ $incripcion->Apellido_Paterno }}</td>
                        <td>{{ $incripcion->Telefono }}</td>
                        <td>{{ $incripcion->disciplina->Descripcion }}</td>
                        <td>{{ $incripcion->Fecha_inicio}}</td>
                        <td>{{ $incripcion->Fecha_Fin}}</td>
                        <td>{{ $incripcion->pago_total }}</td>
                        <td>
                            <form method="post" action="{{ url('/admin/incripcion/'.$incripcion->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <a href="{{ url('/admin/incripcion/'.$incripcion->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>                    
                    @endforeach
                </tbody>
            </table>
            {{ $incripcions->appends(['search' => $search])->links() }}
        </div>
    </div>
@endsection