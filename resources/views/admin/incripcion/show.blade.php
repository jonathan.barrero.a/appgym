@extends('home')

@section('content')
    <div class="row text-center">    
        <div class="col-md-6">
            <div class="card">
                <form action="{{ url('/admin/incripcion/show/day') }}" method="get">        
                    <div class="row" style="padding-left:180px;">            
                        <div class="col-md-4">  
                            <div class="form-group label-floating">              
                                <label class="control-label">Fecha Inicio</label>
                                <input id="dateInit" type="text" class="form-control border-input" name="Fecha_inicio" value="{{ $FechaInicio }}">
                            </div>  
                        </div>
                        <div class="col-md-4">  
                            <div class="form-group label-floating">              
                                <label class="control-label">Fecha Fin</label>
                                <input id="dateFin" type="text" class="form-control border-input datepicker" name="Fecha_Fin" value="{{ $FechaFin }}">
                            </div>  
                        </div>           
                    </div>
                    <hr>
                    <div class="row text-center">
                        <div class="col-sm-12 form-group">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                            <a href="{{ url('/admin/incripcion/show/day') }}" class="btn btn-warning"><i class="fas fa-broom"></i>Limpiar</a>
                            <a href="{{ url('/admin/incripcion/export/'.$FechaInicio.'/'.$FechaFin) }}" class="btn btn-success"><i class="fas fa-broom"></i>Exportar</a>
                        </div>
                    </div>            
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="col-md-4">
                    <div class="form-group label-floating">              
                        <label class="control-label">Total de Alumnos</label>
                        <input id="dateInit" type="text" class="form-control border-input" value="{{ $montoAlumnos }}">
                    </div>  
                </div>
                <div class="col-md-4">
                    <div class="form-group label-floating">              
                        <label class="control-label">Monto Total bs.</label>
                        <input id="dateInit" type="text" class="form-control border-input" value="{{ $montoTotal }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top:50px;">
        <div class="card">
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>                    
                        <th>Numero Identificacion</th>
                        <th>Nombre</th>                    
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>                        
                        <th>Disciplina</th>
                        <th>Monto</th>
                        <th>Creacion</th>
                    </thead>
                    <tbody>
                        @foreach($incripcions as $incripcion)
                        <tr>                        
                            <td>{{ $incripcion->nro_identificacion }}</td>
                            <td>{{ $incripcion->Nombre }}</td>
                            <td>{{ $incripcion->Apellido_Paterno }}</td>
                            <td>{{ $incripcion->Apellido_Materno }}</td>                            
                            <td>{{ $incripcion->disciplina->Descripcion . ' ' . $incripcion->disciplina->hora_inicio . ' ' . $incripcion->disciplina->hora_fin }}</td>
                            <td>{{ $incripcion->pago_total }}</td>
                            <td>{{ $incripcion->created_at}}</td>                                            
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>
                {{ $incripcions->links() }}
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
        $("#dateFin").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>

@endsection