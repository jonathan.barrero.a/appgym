@extends('home')

@section('content')

@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="card">
                <div class="header">
                    <h3 class="title text-center"> Actualizar cliente {{ $cliente->Nombre }} </h3>
                </div>
                <div class="content">
                    <form method="post" action=" {{ url('/admin/incripcion/'.$incripcion->id) }} ">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nro CI</label>
                                    <input type="text" placeholder="Nro de Identificacion" class="form-control border-input" name="nro_identificacion" value="{{ $cliente->nro_identificacion }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" placeholder="Nombre cliente" class="form-control border-input" name="Nombre" value="{{ $cliente->Nombre }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido Paterno</label>
                                    <input type="text" placeholder="Apellido Paterno" class="form-control border-input" name="Apellido_Paterno" value="{{ $cliente->Apellido_Paterno }}">
                                </div> 
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido Materno</label>
                                    <input type="text" placeholder="Apellido Materno" class="form-control border-input" name="Apellido_Materno" value="{{ $cliente->Apellido_Materno }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Telefono</label>
                                    <input type="text" placeholder="Telefono" class="form-control border-input" name="Telefono" value="{{ $cliente->Telefono }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Direccion</label>
                                        <input type="text" placeholder="Direccion" class="form-control border-input" name="Direccion" value="{{ $cliente->Direccion }}">
                                    </div> 
                            
                            </div>
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="text" placeholder="Email" class="form-control border-input" name="Email" value="{{ $cliente->Email }}">
                                    </div>               
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Monto</label>
                                    <input type="text" placeholder="Monto" class="form-control border-input" name="pago_total" value="{{ $incripcion->pago_total }}">
                                </div>  
                            </div>
                            <div class="col-sm-4">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Tipo de Pago</label>
                                    <select name="pago_id" class="form-control border-input">
                                    <option disabled selected>Tipo de Pago</option>
                                    @foreach($pagos as $pago)
                                        <option value="{{ $pago->id }}" @if($pago->id == $incripcion->pago_id) selected='selected' @endif>{{ $pago->Descripcion }}</option>
                                    @endforeach
                                    </select>
                                </div>  
                            </div>
                            <div class="col-sm-5">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Disciplina</label>
                                    <select name="disciplina_id" class="form-control border-input">
                                            <option disabled selected>Seleccione Disciplina</option>
                                        @foreach($disciplinas as $disciplina)                                            
                                            <option value="{{ $disciplina->id }}" @if($disciplina->id == $incripcion->disciplina_id) selected='selected' @endif>{{ $disciplina->Descripcion }}</option>
                                        @endforeach
                                    </select>
                                </div>  
                            </div>
                        </div>    
                        <hr>                                        
                        <div class="row" style="padding-left:350px;">
                            <div class="col-sm-3">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Fecha Inicio</label>
                                    <input id="dateInit" type="text" class="form-control border-input" name="Fecha_inicio" @if($incripcion->Fecha_inicio)  value="{{ \Carbon\Carbon::parse($incripcion->Fecha_inicio)->format('Y-m-d')}}" @else value="{{$incripcion->Fecha_inicio }}" @endif>
                                </div>  
                            </div>
                            <div class="col-sm-3">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Fecha Fin</label>
                                    <input id="dateFin" type="text" class="form-control border-input" name="Fecha_Fin" @if($incripcion->Fecha_Fin)  value="{{ \Carbon\Carbon::parse($incripcion->Fecha_Fin)->format('Y-m-d')}}" @else value="{{$incripcion->Fecha_Fin }}" @endif>
                                </div>  
                            </div>
                        </div>
                    
                        <div class="row text-center">                 
                                <button class="btn btn-primary">Actualizar Datos</button>                        
                        </div>
                    </form>
                </div>
        </div>

@endsection

@section('scripts')

    <script>
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
        $("#dateFin").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>

@endsection