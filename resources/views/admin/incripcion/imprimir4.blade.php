<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Impresion</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    p {
        font-weight: bold;
    }
    span {
        font-weight: bold;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title" style="width:30%;">
                                <img src="{{ url('/img/logogym.png') }}" alt="Thumbnail Image" class="img-raised img-circle" style="width:70px; height:70px;"> 
                            </td>                            
                            <td style="width:40%;text-align: center; vertical-align: middle;">
                                <h2>RECIBO</h2>
                            </td>
                            <td style="with:30%;text-align: right">
                                <p>Nro Recibo # : {{ $imprimir->id }}<br></p>
                                Fecha Registro: {{ $fecha }}                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Do Factory.<br>
                                Av. Beni<br>
                                Entre 7 y 8 anillo, Telf:3-3700758
                            </td>
                            
                            <td>
                            {{$imprimir->Nombre}}<br>
                            {{$imprimir->Apellido_Paterno}}<br>
                            {{$imprimir->Apellido_Materno}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Pago de la deuda restante
                </td>
                
                <td>
                    Monto 
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    {{$imprimir->disciplina->Descripcion . ' - ' . $imprimir->disciplina->hora_inicio . ' - ' . $imprimir->disciplina->hora_fin }}
                </td>
                
                <td>
                {{ $deuda->monto_deuda }} Bs.
                </td>
            </tr>
            
            
            <tr>
                <td style="padding-left:300px; padding-top:50px;">
                    Firma:
                </td>
            </tr>
        </table>
    </div>
    <br>
    <br>
    <div class="invoice-box">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="2">
                <table>
                    <tr>
                        <td class="title" style="width:30%;">
                            <img src="{{ url('/img/logogym.png') }}" alt="Thumbnail Image" class="img-raised img-circle" style="width:70px; height:70px;"> 
                        </td>                            
                        <td style="width:40%;text-align: center; vertical-align: middle;">
                            <h2>RECIBO</h2>
                        </td>
                        <td style="with:30%;text-align: right">
                            <p>Nro Recibo # : {{ $imprimir->id }}<br></p>
                            Fecha Registro: {{ $fecha }}                                
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr class="information">
            <td colspan="2">
                <table>
                    <tr>
                        <td>
                            Do Factory.<br>
                            Av. Beni<br>
                            Entre 7 y 8 anillo, Telf:3-3700758
                        </td>
                        
                        <td>
                        {{$imprimir->Nombre}}<br>
                        {{$imprimir->Apellido_Paterno}}<br>
                        {{$imprimir->Apellido_Materno}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        <tr class="heading">
            <td>
                Pago de la deuda restante
            </td>
            
            <td>
                Monto 
            </td>
        </tr>
        
        <tr class="details">
            <td>
                {{$imprimir->disciplina->Descripcion . ' - ' . $imprimir->disciplina->hora_inicio . ' - ' . $imprimir->disciplina->hora_fin }}
            </td>
            
            <td>
            {{ $deuda->monto_deuda }} Bs.
            </td>
        </tr>
        
        
        <tr>
            <td style="padding-left:300px; padding-top:50px;">
                Firma:
            </td>
        </tr>
    </table>
</div>

</body>
</html>