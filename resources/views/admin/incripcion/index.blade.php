@extends('home')

@section('content')
    <div class="row">
        <div class="row text-center">
            <a href="{{ url('/admin/incripcion/create') }}" class="btn btn-primary">Agregar Nuevo Cliente</a>                                                            
        </div>

        <div class="row" style="padding-top:30px; padding-bottom:50px;">
            <form action="{{ url('/admin/incripcion' )}}" method="get">                
                <div class="col-md-6">                    
                    <input type="text" class="form-control" placeholder="Buscar Cliente" name="search" aria-label="buscar" aria-describedby="basic-addon1">
                </div>
            </form>
        </div>

        <div class="card">
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>                    
                        <th>Numero Identificacion</th>
                        <th>Nombre</th>                    
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($incripcions as $incripcion)
                        <tr>                        
                            <td>{{ $incripcion->nro_identificacion }}</td>
                            <td>{{ $incripcion->Nombre }}</td>
                            <td>{{ $incripcion->Apellido_Paterno }}</td>
                            <td>{{ $incripcion->Apellido_Materno }}</td>
                            <td>{{ $incripcion->Telefono }}</td>                                                                                                
                            <td>{{ $incripcion->Email }}</td>
                            <td>
                                <form method="post" action="{{ url('/admin/incripcion/'.$incripcion->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/incripcion/reportcliente/'.$incripcion->id) }}" title="Ver Datos del Cliente" class="btn btn-success btn-simple btn-lg">
                                            <i class="fa fa-address-card"></i>
                                        </a>                                                                        
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>
                {{ $incripcions->appends(['search' => $search])->links() }}
            </div>
        </div>
    </div>
@endsection

