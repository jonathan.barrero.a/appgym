@extends('home')

@section('content')      

    <div class="row">
        <div class="card">
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>                    
                        <th>Numero Identificacion</th>
                        <th>Nombre</th>                    
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Deuda Monto</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($incripcions as $incripcion)
                        <tr>                        
                            <td>{{ $incripcion->nro_identificacion }}</td>
                            <td>{{ $incripcion->Nombre }}</td>
                            <td>{{ $incripcion->Apellido_Paterno }}</td>
                            <td>{{ $incripcion->Apellido_Materno }}</td>
                            <td>{{ $incripcion->Telefono }}</td>                                                                                                
                            <td>{{ $incripcion->Email }}</td>
                            <td>
                                <form method="post" action="{{ url('/admin/incripcion/'.$incripcion->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/incripcion/reportcliente/'.$incripcion->id) }}" title="Editar" class="btn btn-success btn-simple btn-lg">
                                            <i class="fa fa-address-card"></i>
                                        </a>                                                                        
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>
                {{ $incripcions->appends(['search' => $search])->links() }}
            </div>
        </div>
    </div>    

@endsection