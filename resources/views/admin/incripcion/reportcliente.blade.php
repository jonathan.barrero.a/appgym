@extends('home')

@section('content')
    <div class="row">
        <div class="row text-center" style="padding-bottom: 50px;">
            <a href="{{ url('/admin/incripcion/renovar/'.$cliente_id) }}" class="btn btn-primary">Renovar Cliente</a>                                                            
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="assets/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <img class="avatar border-white" src="{{ url('/img/use-default.png') }}" alt="..."/>
                            <h4 class="title">Foto Cliente<br />                                
                            </h4>
                        </div>                        
                    </div>
                    <hr>
                    <div class="text-center">
                        {{ $cliente->nro_identificacion}}
                        <hr>
                        {{ $cliente->Nombre }}
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-7">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="cliente-tab" data-toggle="tab" href="#cliente" role="tab" aria-controls="cliente" aria-selected="true">Cliente</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="deuda-tab" data-toggle="tab" href="#deuda" role="tab" aria-controls="deuda" aria-selected="false">Deudas Cliente</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade" id="cliente" role="tabpanel" aria-labelledby="cliente-tab">
                            <div class="card">
                                <div class="content table-responsive table-full-width">
                                    <table class="table table-striped">
                                        <thead>                                                    
                                            <th>Disciplina</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>  
                                            <th>Monto Total</th>                  
                                            <th>Estado</th>        
                                            <th>Accion</th>            
                                        </thead>
                                        <tbody>
                                            @foreach($incripcions as $incripcion)
                                            <tr>                                                                                    
                                                <td>{{ $incripcion->disciplina->Descripcion . ' - ' . $incripcion->disciplina->hora_inicio . ' - ' . $incripcion->disciplina->hora_fin }}</td>                                                                                                
                                                @if($incripcion->Fecha_inicio)
                                                    <td>{{ \Carbon\Carbon::parse($incripcion->Fecha_inicio)->format('d-m-Y') }}</td>
                                                @else
                                                    <td>{{ $incripcion->Fecha_inicio }}</td>
                                                @endif
                                                @if($incripcion->Fecha_Fin)
                                                    <td>{{ \Carbon\Carbon::parse($incripcion->Fecha_Fin)->format('d-m-Y') }}</td>                                                
                                                @else
                                                    <td>{{ $incripcion->Fecha_Fin }}</td>
                                                @endif  
                                                <td>{{ $incripcion->pago_total }}</td>                                              
                                                <td>{{ $incripcion->Estado_incripcion }}</td>                                                
                                                <td>
                                                    <a href="{{ url('/admin/incripcion/'.$incripcion->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                                        <i class="fa fa-edit"></i>
                                                    </a>         
                                                    <a href="{{ url('/admin/incripcion/imprimir3/'.$incripcion->id) }}" target="_blank" title="Imprimir Recibo" class="btn btn-primary btn-simple btn-lg">
                                                        <i class="fa fa-print"></i>
                                                    </a>                                        
                                                    <button id="btnDeuda" type="button" title="Agregar deuda al cliente" class="btn btn-danger btn-simple btn-lg" data-toggle="modal" data-target="#deudamodal" value="{{ $incripcion->id }}" onclick="valuedeuda({{$incripcion->id}})"><i class="fa fa-file"></i></button>
                                                </td>
                                            </tr>                    
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $incripcions->links() }}
                                </div>
                            </div>                
                        </div>
                        <div class="tab-pane fade" id="deuda" role="tabpanel" aria-labelledby="deuda-tab">                            
                                <div class="row">
                                    <div class="card">
                                        <div class="content table-responsive table-full-width">
                                            <table class="table table-striped">
                                                <thead>                    
                                                    <th>Nombre</th>
                                                    <th>Disciplina</th>                                                                                                                            
                                                    <th>Deuda Monto</th>                                                    
                                                    <th>Estado Deuda</th>
                                                    <th>Acciones</th>
                                                </thead>
                                                <tbody>
                                                    @foreach($deudas as $deuda)
                                                    <tr>                        
                                                        <td>{{ $deuda->cliente->Nombre }}</td>
                                                        <td>{{ $deuda->disciplina->Descripcion. ' - ' . $deuda->disciplina->hora_inicio. ' - ' . $deuda->disciplina->hora_fin }}</td>
                                                        <td>{{ $deuda->monto_deuda }}</td>
                                                        <td>{{ $deuda->estado_deuda }}</td>
                                                        <td>
                                                            <form method="post" action="{{ url('/admin/incripcion/deuda/'.$deuda->id) }}">
                                                                {{ csrf_field() }}
                                                                {{ method_field('DELETE') }}
                                                                <a href="{{ url('/admin/incripcion/imprimir4/'.$incripcion->id) }}" target="_blank" title="Imprimir Recibo" class="btn btn-primary btn-simple btn-lg">
                                                                    <i class="fa fa-print"></i>
                                                                </a>   
                                                                @if($deuda->estado_deuda <> 'Pagado') 
                                                                <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                                                @endif                                                                
                                                            </form>
                                                        </td>                                                                                                                
                                                    </tr>                    
                                                    @endforeach
                                                </tbody>
                                            </table>                                            
                                        </div>
                                    </div>
                                </div>                              
                        </div>
                    </div>                    
            </div>
        </div>
    </div>





        
    
@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="deudamodal" tabindex="-1" role="dialog" aria-labelledby="deudaModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deudaModalLabel">Registrar Deuda del Cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">                    
                        <div class="content">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ url('/admin/incripcion/deuda') }}" method="post">
                                {{ csrf_field() }}                                                 
                                        <input id="hiddendeuda" type="hidden" name="incripcion_id" value="" />                                                             
                                        <div class="col-md-6">
                                            <div class="form-group label-floating">                                            
                                                <input type="text" placeholder="Registrar Monto de la deuda" class="form-control border-input" name="monto_deuda" value="{{ old('monto_deuda') }}">
                                            </div> 
                                        </div>                                                                
                                    <button class="btn btn-success">Registrar Pago</button>                                                            
                            </form>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function valuedeuda(id)
        {
            document.getElementById('hiddendeuda').value = id
        }
    </script>
@endsection