@extends('home')

@section('content')         
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">        
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="assets/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <img class="avatar border-white" src="{{ url('/img/use-default.png') }}" alt="..."/>
                            <h4 class="title">Foto Cliente<br />                                
                            </h4>
                        </div>                        
                    </div>
                    <hr>
                    <div class="text-center">
                        
                    </div>
                </div>
            </div>  

            <div class="col-lg-8 col-md-7">
                <div class="card">
                        <div class="header">
                            <h3 class="title text-center"> Registrar Nuevo Cliente </h3>
                        </div>
                        <div class="content">
                            <form method="post" action=" {{ url('/admin/incripcion') }} ">
                            {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nro CI</label>
                                            <input type="text" placeholder="Nro de Identificacion" class="form-control border-input" name="nro_identificacion" value="{{ old('nro_identificacion') }}">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nombre</label>
                                            <input type="text" placeholder="Nombre cliente" class="form-control border-input" name="Nombre" value="{{ old('Nombre') }}">
                                        </div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Apellido Paterno</label>
                                            <input type="text" placeholder="Apellido Paterno" class="form-control border-input" name="Apellido_Paterno" value="{{ old('Apellido_Paterno') }}">
                                        </div> 
                                    </div>                
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Apellido Materno</label>
                                            <input type="text" placeholder="Apellido Materno" class="form-control border-input" name="Apellido_Materno" value="{{ old('Apellido_Materno') }}">
                                        </div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Telefono</label>
                                            <input type="text" placeholder="Telefono" class="form-control border-input" name="Telefono" value="{{ old('Telefono') }}">
                                        </div> 
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Direccion</label>
                                                <input type="text" placeholder="Direccion" class="form-control border-input" name="Direccion" value="{{ old('Direccion') }}">
                                            </div> 
                                    
                                    </div>
                                    <div class="col-sm-6">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email</label>
                                                <input type="text" placeholder="Email" class="form-control border-input" name="Email" value="{{ old('Email') }}">
                                            </div>               
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Monto</label>
                                            <input type="text" placeholder="Monto" class="form-control border-input" name="pago_total" value="{{ old('pago_total') }}">
                                        </div>  
                                    </div>
                                    <div class="col-sm-4">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Tipo de Pago</label>
                                            <select name="pago_id" class="form-control border-input">
                                            <option disabled selected>Tipo de Pago</option>
                                            @foreach($pagos as $pago)
                                                <option value="{{ $pago->id }}">{{ $pago->Descripcion }}</option>
                                            @endforeach
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="col-sm-5">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Disciplina</label>
                                            <select name="disciplina_id" class="form-control border-input">
                                                    <option disabled selected>Seleccione Disciplina</option>
                                                @foreach($disciplinas as $disciplina)                                            
                                                    <option value="{{ $disciplina->id }}">{{ $disciplina->Descripcion . ' - ' . $disciplina->hora_inicio . ' - ' . $disciplina->hora_fin }}</option>
                                                @endforeach
                                            </select>
                                        </div>  
                                    </div>
                                </div>                                           
                                <hr> 
                                <div class="row" style="padding-left:200px;">
                                    <div class="col-sm-3">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Fecha Inicio</label>
                                            <input id="dateInit" type="text" class="form-control border-input" name="Fecha_inicio" value="{{ old('Fecha_inicio') }}">
                                        </div>  
                                    </div>
                                    <div class="col-sm-3">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Fecha Fin</label>
                                            <input id="dateFin" type="text" class="form-control border-input datepicker" name="Fecha_Fin" value="{{ old('Fecha_Fin') }}">
                                        </div>  
                                    </div>
                                </div>
                            
                                <div class="row text-center" style="padding-top:20px;">                 
                                        <button class="btn btn-primary">Registrar Pago</button>                        
                                </div>
                            </form>
                        </div>
                </div>        
            </div>        
        </div>
@endsection

@section('scripts')

    <script>
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
        $("#dateFin").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>

@endsection