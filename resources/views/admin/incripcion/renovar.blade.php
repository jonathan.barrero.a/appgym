@extends('home')

@section('content')

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="card">
                <div class="header">
                    <h3 class="title text-center"> Renovar cliente {{ $cliente->Nombre }} </h3>
                </div>
                <hr>
                <div class="content">
                    <form method="post" action=" {{ url('/admin/incripcion/renovar') }} ">
                    {{ csrf_field() }}                    
                        
                        <!-- Colocar el hidden oculto del cliente id -->
                        <input type="hidden" name="cliente_id" value="{{ $cliente->id }}">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nro CI</label>
                                    <input type="text" placeholder="Nro de Identificacion" class="form-control border-input" name="nro_identificacion" value="{{ $cliente->nro_identificacion }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" placeholder="Nombre cliente" class="form-control border-input" name="Nombre" value="{{ $cliente->Nombre }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido Paterno</label>
                                    <input type="text" placeholder="Apellido Paterno" class="form-control border-input" name="Apellido_Paterno" value="{{ $cliente->Apellido_Paterno }}">
                                </div> 
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido Materno</label>
                                    <input type="text" placeholder="Apellido Materno" class="form-control border-input" name="Apellido_Materno" value="{{ $cliente->Apellido_Materno }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Telefono</label>
                                    <input type="text" placeholder="Telefono" class="form-control border-input" name="Telefono" value="{{ $cliente->Telefono }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Direccion</label>
                                        <input type="text" placeholder="Direccion" class="form-control border-input" name="Direccion" value="{{ $cliente->Direccion }}">
                                    </div> 
                            
                            </div>
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input type="text" placeholder="Email" class="form-control border-input" name="Email" value="{{ $cliente->Email }}">
                                    </div>               
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Monto</label>
                                    <input type="text" placeholder="Monto" class="form-control border-input" name="pago_total" value="{{ old('pago_total') }}">
                                </div>  
                            </div>
                            <div class="col-sm-4">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Tipo de Pago</label>
                                            <select name="pago_id" class="form-control border-input">
                                            <option disabled selected>Tipo de Pago</option>
                                            @foreach($pagos as $pago)
                                                <option value="{{ $pago->id }}">{{ $pago->Descripcion }}</option>
                                            @endforeach
                                            </select>
                                        </div>  
                            </div>
                            <div class="col-sm-5">  
                                        <div class="form-group label-floating">              
                                            <label class="control-label">Disciplina</label>
                                            <select name="disciplina_id" class="form-control border-input">
                                                    <option disabled selected>Seleccione Disciplina</option>
                                                @foreach($disciplinas as $disciplina)                                            
                                                    <option value="{{ $disciplina->id }}">{{ $disciplina->Descripcion . ' - ' . $disciplina->hora_inicio . ' - ' . $disciplina->hora_fin }}</option>
                                                @endforeach
                                            </select>
                                        </div>  
                            </div>
                        </div>    
                        <hr>                                        
                        <div class="row" style="padding-left:350px;">
                            <div class="col-sm-3">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Fecha Inicio</label>
                                    <input id="dateInit" type="text" class="form-control border-input" name="Fecha_inicio" value="{{ old('Fecha_inicio') }}">
                                </div>  
                            </div>
                            <div class="col-sm-3">  
                                <div class="form-group label-floating">              
                                    <label class="control-label">Fecha Fin</label>
                                    <input id="dateFin" type="text" class="form-control border-input datepicker" name="Fecha_Fin" value="{{ old('Fecha_Fin') }}">
                                </div>  
                            </div>
                        </div>
                    
                        <div class="row text-center">                 
                                <button class="btn btn-primary">Renovar Cliente</button>                        
                        </div>
                    </form>
                </div>
        </div>

@endsection

@section('scripts')

    <script>
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
        $("#dateFin").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>

@endsection