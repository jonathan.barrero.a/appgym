@extends('home')

@section('content')

    <div class="row text-center">
        <a href="{{ url('/admin/inventario/create') }}" class="btn btn-primary">Agregar Nuevo a Inventario</a>
    </div>

    <div class="row" style="padding-top:30px;">
        <form action="{{ url('/admin/inventario' )}}" method="get">
            <div class="form-group">
                <input type="text" name="search" class="form-control" placeholder="Buscar en el inventario.">
            </div>
        </form>
    </div>

    <div class="row">
        <div class="card">        
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Nombre Inventario</th>
                        <th>Descripcion</th>
                        <th>Codigo Producto</th>
                        <th>Costo unitario</th>
                        <th>Stock</th>                        
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach ($inventarios as $inventario)
                        <tr>
                            <td>{{ $inventario->id }}</td>
                            <td>{{ $inventario->nombre }}</td>
                            <td>{{ $inventario->Descripcion }}</td>
                            <td>{{ $inventario->codigo_producto }}</td>
                            <td>{{ $inventario->precio_costo }}</td>
                            <td>{{ $inventario->stock }}</td>                                                        
                            <td>
                                <form method="post" action="{{ url('/admin/inventario/'.$inventario->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/inventario/'.$inventario->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection