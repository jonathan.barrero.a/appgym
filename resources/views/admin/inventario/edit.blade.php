@extends('home')

@section('content')

<div class="container">        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <img class="avatar border-white" src="{{ url('/img/producto.png') }}" alt="..."/>
                            <h4 class="title">Foto Inventario<br />                                
                            </h4>
                        </div>                        
                    </div>
                    <hr>
                    <div class="text-center">
                        
                    </div>
                </div>
            </div>  

            <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h3 class="title text-center"> Actualizar Inventario </h3>
                </div>
                <div class="content">
                    <form method="post" action=" {{ url('/admin/inventario/'.$inventario->id) }} ">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre del articulo</label>
                                    <input type="text" placeholder="Nombre para inventario" class="form-control" name="nombre" value="{{ $inventario->nombre }}">
                                </div> 
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-sm-12">                            
                                <div class="form-group label-floating">
                                    <label class="control-label">Descripcion</label>
                                    <textarea type="text" placeholder="Descripcion del inventario" class="form-control" rows="3" name="Descripcion" >{{ $inventario->Descripcion }}</textarea>
                                </div>                             
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Codigo para inventario</label>
                                    <input type="text" placeholder="Codigo para inventario puede ir vacio." class="form-control" name="codigo_producto" value="{{ $inventario->codigo_producto }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Precio del articulo</label>
                                    <input type="text" placeholder="Precio del articulo" class="form-control" name="precio_costo" value="{{ $inventario->precio_costo }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">stock</label>
                                        <input type="number" placeholder="stock del producto" class="form-control" name="stock" value="{{ $inventario->stock }}">
                                    </div> 
                            
                            </div>
                        </div>                        
                        <div class="row text-center" style="padding-top:20px;">
                            <button class="btn btn-primary">Actualizar Articulo</button>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
</div>

@endsection