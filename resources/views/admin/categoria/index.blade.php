@extends('home')

@section('content')

<div class="row">
        <div class="row text-center">
            <a href="{{ url('/admin/categoria/create') }}" class="btn btn-primary">Agregar Nueva Categoria</a>
        </div>

        <div class="row" style="padding-top:30px;">
            <form action="{{ url('/admin/categoria' )}}" method="get">
                <div class="form-group">
                    <input type="text" name="search" class="form-control" placeholder="Buscar Categoria.">
                </div>
            </form>
        </div>

        <div class="card">
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>Id</th>
                        <th>Descripcion</th>                                        
                        <th class="col-md-2">Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($categorias as $categoria)
                        <tr>
                            <td>{{ $categoria->id }}</td>
                            <td>{{ $categoria->Descripcion }}</td>                        
                            <td>
                                <form method="post" action="{{ url('/admin/categoria/'.$categoria->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/categoria/'.$categoria->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>
                {{ $categorias->appends(['search' => $search])->links() }}
            </div>
        </div>
    </div>

@endsection