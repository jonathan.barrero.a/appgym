@extends('home')

@section('content')

    @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <div class="card">
            <div class="header">
                <h3 class="title text-center"> Registrar Nueva Categoria </h3>
            </div>
            <hr>
            <div class="content">
                <form method="post" action=" {{ url('/admin/categoria') }} ">
                {{ csrf_field() }}
                    <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre Categoria</label>
                                    <input type="text" placeholder="Nombre Categoria" class="form-control border-input" name="Descripcion" value="{{ old('Descripcion') }}">
                                </div> 
                            </div>
                    </div>                    
                    <div class="row text-center" style="padding-top:20px;">                
                            <button class="btn btn-primary">Registrar Categoria</button>                        
                    </div>
                </form>
            </div>

    </div>

@endsection