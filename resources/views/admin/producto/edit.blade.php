@extends('home')

@section('content')

<div class="container">        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <img class="avatar border-white" src="{{ url('/img/producto.png') }}" alt="..."/>
                            <h4 class="title">Foto Producto<br />                                
                            </h4>
                        </div>                        
                    </div>
                    <hr>
                    <div class="text-center">
                        
                    </div>
                </div>
            </div>  

            <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h3 class="title text-center"> Editar Producto {{ $producto->nombre }} </h3>
                </div>
                <div class="content">
                    <form method="post" action=" {{ url('/admin/producto/'.$producto->id) }} ">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre Producto</label>
                                    <input type="text" placeholder="Nombre del producto" class="form-control" name="nombre" value="{{ $producto->nombre }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Código Producto</label>
                                    <input type="text" placeholder="Código del producto" class="form-control" name="codigo_producto" value="{{ $producto->codigo_producto }}">
                                </div> 
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Precio Compra</label>
                                    <input type="text" placeholder="Precio de compra" class="form-control" name="precio_costo" value="{{ $producto->precio_costo }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Precio Venta</label>
                                    <input type="text" placeholder="Precio de venta" class="form-control" name="precio_venta" value="{{ $producto->precio_venta }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">stock</label>
                                        <input type="text" placeholder="stock del producto" class="form-control" name="stock" value="{{ $producto->stock }}">
                                    </div> 
                            
                            </div>
                            <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">stock mínimo</label>
                                        <input type="text" placeholder="stock mínimo para la alerta" class="form-control" name="stock_min" value="{{ $producto->stock_min }}">
                                    </div>                             
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-6">
                                <div class="form-group label-floating">              
                                    <label class="control-label">Tipo de Categoria</label>
                                    <select name="categoria_id" class="form-control">
                                        @foreach ($categorias as $categoria)       
                                            <option value="{{ $categoria->id }}" @if($producto->categoria_id == $categoria->id) selected='selected' @endif>{{ $categoria->Descripcion }}</option>                                        
                                        @endforeach
                                    </select>
                                </div>               
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">              
                                    <label class="control-label">Tipo de Unidad</label>
                                    <select id="unidades" name="unidades_id" class="form-control">
                                        @foreach ($unidades as $unidad)                                        
                                            <option value="{{ $unidad->id }}" @if($unidad->id == $producto->unidad_id) selected='selected' @endif>{{ $unidad->Descripcion }}</option>
                                        @endforeach
                                    </select>
                                </div>               
                            </div>
                        </div>
                        <div class="row text-center" style="padding-top:20px;">
                            <button class="btn btn-primary">Actualizar Producto</button>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
</div>

@endsection

