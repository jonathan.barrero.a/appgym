@extends('home')

@section('content')

    <div class="row text-center">
        <a href="{{ url('/admin/producto/create') }}" class="btn btn-primary">Agregar Nuevo Producto</a>
    </div>

    <div class="row" style="padding-top:30px;">
        <form action="{{ url('/admin/producto' )}}" method="get">
            <div class="form-group">
                <input type="text" name="search" class="form-control" placeholder="Buscar Producto.">
            </div>
        </form>
    </div>

    <div class="row">
        <div class="card">        
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Nombre Producto</th>
                        <th>Codigo Producto</th>
                        <th>Precio Costo</th>
                        <th>Precio Venta</th>
                        <th>Stock</th>                        
                        <th>Categoria</th>
                        <th>Unidades</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach ($productos as $producto)
                        <tr>
                            <td>{{ $producto->id }}</td>
                            <td>{{ $producto->nombre }}</td>
                            <td>{{ $producto->codigo_producto }}</td>
                            <td>{{ $producto->precio_costo }}</td>
                            <td>{{ $producto->precio_venta }}</td>
                            <td>{{ $producto->stock }}</td>
                            <td>{{ $producto->categoria->Descripcion }}</td>
                            <td>{{ $producto->unidades->Descripcion }}</td>
                            <td>
                                <form method="post" action="{{ url('/admin/producto/'.$producto->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/producto/'.$producto->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection