@extends('home')

@section('content')

<div class="row">
        <div class="row text-center">
            <a href="{{ url('/admin/disciplina/create') }}" class="btn btn-primary">Agregar Nueva Disciplina</a>
        </div>

        <div class="row" style="padding-top:30px;">
                    <form action="{{ url('/admin/disciplina' )}}" method="get">
                        <div class="form-group">
                            <input type="text" name="search" class="form-control" placeholder="Buscar Disciplina.">
                        </div>
                    </form>
        </div>

        <div class="content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>
                    <th>Id</th>
                    <th>Nombre disciplina</th>
                    <th>Hora Inicio</th>                    
                    <th>Hora Fin</th>                    
                    <th>Acciones</th>
                </thead>
                <tbody>
                    @foreach ($disciplinas as $disciplina)
                    <tr>
                        <td>{{ $disciplina->id }}</td>
                        <td>{{ $disciplina->Descripcion }}</td>
                        <td>{{ $disciplina->hora_inicio }}</td>                        
                        <td>{{ $disciplina->hora_fin }}</td>
                        <td>{{ $disciplina->created_at}}</td>                        
                        <td>
                            <form method="post" action="{{ url('/admin/disciplina/'.$disciplina->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <a href="{{ url('/admin/disciplina/'.$disciplina->id)}}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>                    
                    @endforeach
                </tbody>
            </table>
            {{ $disciplinas->appends(['search' => $search])->links() }}
        </div>
</div>

@endsection