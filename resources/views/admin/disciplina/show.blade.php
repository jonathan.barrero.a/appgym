@extends('home')

@section('content')

    <div class="row text-center">    
        <div class="col-md-6">
        <div class="card">
        <form action="{{ url('/admin/incidencias/show') }}" method="get">        
            <div class="row" style="padding-left:180px;">            
                <div class="col-md-4">  
                    <div class="form-group label-floating">              
                        <label class="control-label">Fecha Inicio</label>
                        <input id="dateInit" type="text" class="form-control border-input" name="Fecha_inicio" value="{{ $FechaInicio }}">
                    </div>  
                </div>
                <div class="col-md-4">  
                    <div class="form-group label-floating">              
                        <label class="control-label">Fecha Fin</label>
                        <input id="dateFin" type="text" class="form-control border-input datepicker" name="Fecha_Fin" value="{{ $FechaFin }}">
                    </div>  
                </div>            
            </div>
            <hr>
            <div class="row text-center">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-primary" type="submit">Buscar</button>
                    <a href="{{ url('/admin/incidencias/show') }}" class="btn btn-warning"><i class="fas fa-broom"></i>Limpiar</a>
                    <a href="{{ url('/admin/incidencias/export/'.$FechaInicio.'/'.$FechaFin) }}" class="btn btn-success"><i class="fas fa-broom"></i>Exportar</a>
                </div>
            </div>            
        </form>
        </div>
        </div>
    </div>

    <div class="row" style="padding-top:50px;">
        <div class="card">
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>Nro. Producto</th>
                        <th>Nombre del producto</th>
                        <th>stock</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Sub Total</th>
                        <th>Fecha de Venta</th>
                        <th>Usuario</th>
                        <th class="col-md-2 text-center">Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($detallecompras as $detallecompra)
                        <tr>
                            <th>{{ $detallecompra->id }}</th>
                            <th>{{ $detallecompra->producto->nombre }}</th>
                            <th>{{ $detallecompra->producto->stock }}</th>
                            <th>{{ $detallecompra->producto->precio_venta }}</th>
                            <th>{{ $detallecompra->cantidad }}</th>
                            <th>{{ $detallecompra->cantidad * $detallecompra->producto->precio_venta  }}</th>
                            <th>{{ \Carbon\Carbon::parse($detallecompra->created_at)->format('d-m-Y') }}</th>
                            <th>{{ $detallecompra->user_id }}</th>
                            <th>
                                <form action="{{ url('/admin/detallecompras/'.$detallecompra->id) }}" method="post">                            
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}                                    
                                    <button id="btnEdit" type="button" class="btn btn-primary btn-simple btn-lg" data-toggle="modal" data-target="#deudaedit" value="{{ $detallecompra->id }}" onclick="valuecantidad({{ $detallecompra->id }}, {{ $detallecompra->cantidad }})"><i class="fa fa-edit"></i></button>                                    
                                    <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>                                    
                                </form>
                            </th>
                        </tr>
                        @endforeach                        
                    </tbody>
                </table>            
            </div>        
        </div>
    </div>


@endsection


@section('scripts')
    <script>
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
        $("#dateFin").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>
@endsection