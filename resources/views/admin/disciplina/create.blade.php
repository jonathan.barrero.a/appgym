@extends('home')

@section('content')

     @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
    @endif

    <div class="card">
            <div class="header">
                <h3 class="title text-center"> Registrar Nueva Disciplina </h3>
            </div>
            <div class="content">
                <form method="post" action=" {{ url('/admin/disciplina') }} ">
                {{ csrf_field() }}
                    <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre Disciplina</label>
                                    <input type="text" placeholder="Nombre Disciplina" class="form-control border-input" name="Descripcion" value="{{ old('Descripcion') }}">
                                </div> 
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="control-label">Hora de Inicio</label>
                            <input id="horaInicio" type="text" placeholder="Hora de inicio hh/mm" class="form-control border-input" name="hora_inicio" value="{{ old('hora_inicio') }}">
                        </div>
                        <div class="col-sm-6">
                            <label class="control-label">Hora de Fin</label>
                            <input id="horaFin" type="text" placeholder="Hora Fin hh/mm" class="form-control border-input" name="hora_fin" value="{{ old('hora_fin') }}">
                        </div>
                    </div>
                    <div class="row text-center" style="padding-top:20px;">                
                            <button class="btn btn-primary">Registrar Disciplina</button>                        
                    </div>
                </form>
            </div>

    </div>


@endsection


@section('scripts')
    <script>        
        var dynamicMask = new IMask(
            document.getElementById('horaInicio'),
            {
                mask: [
                {
                    mask: '00:00'
                },
                {
                    mask: /^[1-4]\d{0,3}$/
                }
                ]
            });

        var dynamicMask = new IMask(
            document.getElementById('horaFin'),
            {
                mask: [
                {
                    mask: '00:00'
                },
                {
                    mask: /^[1-4]\d{0,3}$/
                }
                ]
            });
    </script>
@endsection