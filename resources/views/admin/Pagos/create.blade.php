@extends('home')

@section('content')

        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card">
            <div class="header">
            <h3 class="title text-center"> Registrar Nuevo Metodo Pago </h3>
            </div>
            <div class="content">
            <form method="post" action=" {{ url('/admin/Pagos') }} ">
            {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Tipo de Pago</label>
                            <input type="text" placeholder="Tipo de pago" class="form-control border-input" name="Descripcion" value="{{ old('Descripcion') }}">
                        </div> 
                        <div class="row text-center">                 
                            <button class="btn btn-primary">Registrar Pago</button>                        
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>

@endsection