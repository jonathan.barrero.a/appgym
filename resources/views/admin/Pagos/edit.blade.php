@extends('home')

@section('content')

        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="card">
            <div class="header">
            <h3 class="title text-center"> Editar Pago {{ $pago->Descripcion }} </h3>
            </div>
            <div class="content">
            <form method="post" action=" {{ url('/admin/Pagos/'.$pago->id) }} ">
            {{ csrf_field() }}
            {{ method_field('put') }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Tipo de Pago</label>
                            <input type="text" placeholder="Tipo de pago" class="form-control border-input" name="Descripcion" value="{{ $pago->Descripcion }}">
                        </div> 
                        <div class="row text-center">                 
                            <button class="btn btn-primary">Actualizar Pago</button>                        
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>

@endsection
