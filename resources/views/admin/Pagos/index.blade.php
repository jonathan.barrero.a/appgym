@extends('home')

@section('content')

    <div class="row text-center">
            <a href="{{ url('/admin/Pagos/create') }}" class="btn btn-primary">Agregar Nuevo Metodo de Pago</a>
    </div>


    <div class="row">
        <div class="content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>
                    <th>ID</th>
                    <th>Nombre Pago</th>
                    <th class="col-md-2">Acciones</th>
                </thead>
                <tbody>
                    @foreach ($pagos as $pago)
                    <tr>
                        <td>{{ $pago->id }}</td>
                        <td>{{ $pago->Descripcion }}</td>
                        <td>
                            <form method="post" action=" {{ url('/admin/Pagos/'.$pago->id) }}">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <a href="{{ url('/admin/Pagos/'.$pago->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
@endsection