@extends('home')

@section('content')

    <div class="row">
        <div class="card">        
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>Id</th>
                        <th>Nombre Usuario</th>
                        <th>Cuenta Usuario</th>
                        <th>Estado</th>
                        <th>Acciones</th>                    
                    </thead>
                    <tbody>
                        @foreach($usuarios as $usuario)
                        <tr>
                            <td>{{ $usuario->id }}</td>
                            <td>{{ $usuario->name }}</td>
                            <td>{{ $usuario->email }}</td>
                            <td>Activo</td>                        
                            <td>
                                <form method="post" action="#">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="#" title="Editar" class="btn btn-info btn-simple btn-lg">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="#" title="Editar" class="btn btn-danger btn-simple btn-lg">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                </form>
                            </td>
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>
                {{ $usuarios->links() }}
            </div>
        </div>
    </div>
@endsection