@extends('home')

@section('content')

<div class="row text-center">    
        <div class="col-md-8">
        <div class="card">
            <form action="{{ url('/admin/Instructor/show/day') }}" method="get">        
                <div class="row" style="padding-left:10px; padding-right:10px;">            
                    <div class="col-md-4">  
                        <div class="form-group label-floating">              
                            <label class="control-label">Fecha Inicio</label>
                            <input id="dateInit" type="text" class="form-control border-input" name="Fecha_inicio" value="{{ $FechaInicio }}">
                        </div>  
                    </div>
                    <div class="col-md-4">  
                        <div class="form-group label-floating">              
                            <label class="control-label">Fecha Fin</label>
                            <input id="dateFin" type="text" class="form-control border-input datepicker" name="Fecha_Fin" value="{{ $FechaFin }}">
                        </div>  
                    </div>
                    <div class="col-md-4">  
                        <div class="form-group label-floating">              
                            <label class="control-label">Instructor</label>
                                <select name="instructor_id" class="form-control border-input">
                                    <option disabled selected>Seleccione Instructor</option>
                                    @foreach($totalInstructores as $totalInstructore)                                            
                                        <option value="{{ $totalInstructore->id }}">{{ $totalInstructore->Nombres . ' ' . $totalInstructore->Apellido_Paterno }}</option>
                                    @endforeach
                                </select>
                        </div>
                    </div>            
                </div>
                <hr>
                <div class="row text-center">
                        <div class="col-sm-12 form-group">
                            <button class="btn btn-primary" type="submit">Buscar</button>
                            <a href="{{ url('/admin/Instructor/show/day') }}" class="btn btn-warning"><i class="fas fa-broom"></i>Limpiar</a>
                            <a href="{{ url('/admin/Instructor/export/'.$FechaInicio.'/'.$FechaFin.'/'.$idInstructor) }}" class="btn btn-success"><i class="fas fa-broom"></i>Exportar</a>
                        </div>
                    </div>            
            </form>
        </div>
        </div>                
</div>

    <div class="row" style="padding-top:50px;">
    <div class="card">        
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>Nombre del cliente</th>                        
                        <th>Nro de Carnet</th>                
                        <th>Monto Inscripcion</th>                                
                        <th>Porcentaje %</th>
                        <th>Disciplina</th>                        
                    </thead>
                    <tbody>
                        @foreach($instructors as $instructor)
                        <tr>                            
                            <td>{{ $instructor->Nombre . ' ' . $instructor->Apellido_Paterno . ' ' . $instructor->Apellido_Materno }}</td>
                            <td>{{ $instructor->nro_identificacion }}</td>
                            <td>{{ $instructor->pago_total }}</td>
                            <td>{{ $instructor->porcentaje }}</td>                                 
                            <td>{{ $instructor->disciplina->Descripcion . ' ' . $instructor->disciplina->hora_inicio . ' ' . $instructor->disciplina->hora_fin }}</td>                            
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
        $("#dateFin").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>
@endsection