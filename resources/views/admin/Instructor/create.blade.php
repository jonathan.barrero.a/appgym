@extends('home')

@section('content')

<div class="container">        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card card-user">
                    <div class="image">
                        <img src="assets/img/background.jpg" alt="..."/>
                    </div>
                    <div class="content">
                        <div class="author">
                            <img class="avatar border-white" src="{{ url('/img/use-default.png') }}" alt="..."/>
                            <h4 class="title">Foto Instructor<br />                                
                            </h4>
                        </div>                        
                    </div>
                    <hr>
                    <div class="text-center">
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Cargar foto Instructor.</label>
                            <input type="file" class="form-control-file" id="fotoInstructor">
                        </div>
                    </div>
                </div>
            </div>  

            <div class="col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h3 class="title text-center"> Registrar Nuevo Instructor </h3>
                </div>
                <div class="content">
                    <form method="post" action=" {{ url('/admin/Instructor') }} ">
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombres</label>
                                    <input type="text" placeholder="Nombre del instructor" class="form-control" name="Nombres" value="{{ old('Nombres') }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido Paterno</label>
                                    <input type="text" placeholder="Apellido Paterno" class="form-control" name="Apellido_Paterno" value="{{ old('Apellido_Paterno') }}">
                                </div> 
                            </div>                
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido Materno</label>
                                    <input type="text" placeholder="Apellido Materno" class="form-control" name="Apellido_Materno" value="{{ old('Apellido_Materno') }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">
                                    <label class="control-label">Telefono</label>
                                    <input type="text" placeholder="Telefono" class="form-control" name="Telefono" value="{{ old('Telefono') }}">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Direccion</label>
                                        <input type="text" placeholder="Direccion" class="form-control" name="Direccion" value="{{ old('Direccion') }}">
                                    </div> 
                            
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-3">
                                <div class="form-group label-floating">              
                                    <label class="control-label">Tipo de Pago</label>
                                    <select name="Tipo_pago" class="form-control">
                                        <option value="1">Mensual</option>
                                        <option value="2">Comision</option>
                                    </select>
                                </div>               
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">Porcentaje</label>
                                    <input type="text" placeholder="Porcentaje de comision" class="form-control" name="porcentaje" value="{{ old('porcentaje') }}">
                                </div> 
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group label-floating">              
                                    <label class="control-label">Tipo de Disciplina</label>
                                    <select id="disciplina" name="disciplina_id" class="form-control">
                                        @foreach ($disciplinas as $disciplina)                                        
                                        <option value="{{ $disciplina->id }}">{{ $disciplina->Descripcion . ' - ' . $disciplina->hora_inicio . ' - ' . $disciplina->hora_fin }}</option>
                                        @endforeach
                                    </select>
                                </div>               
                            </div>
                        </div>
                        <div class="row text-center" style="padding-top:20px;">
                            <button class="btn btn-primary">Registrar Instructor</button>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
</div>

@endsection

@section('scripts')
    <script src="/js/choises.min.js'"></script>
    <script>        
        document.addEventListener("DOMContentLoaded", function() {
            const choices = new Choices('#disciplina');
        });
    </script>
@endsection