@extends('home')

@section('content')

    <div class="container">        
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="header">
                <h3 class="title text-center"> Editar Instructor {{ $instructor->Nombres }} </h3>
            </div>
            <div class="content">
                <form method="post" action=" {{ url('/admin/Instructor/'.$instructor->id) }} ">
                {{ csrf_field() }}
                {{ method_field('put') }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Nombres</label>
                                <input type="text" placeholder="Nombre del instructor" class="form-control" name="Nombres" value="{{ $instructor->Nombres }}">
                            </div> 
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Apellido Paterno</label>
                                <input type="text" placeholder="Apellido Paterno" class="form-control" name="Apellido_Paterno" value="{{ $instructor->Apellido_Paterno }}">
                            </div> 
                        </div>                
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Apellido Materno</label>
                                <input type="text" placeholder="Apellido Materno" class="form-control" name="Apellido_Materno" value="{{ $instructor->Apellido_Materno }}">
                            </div> 
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">
                                <label class="control-label">Telefono</label>
                                <input type="text" placeholder="Telefono" class="form-control" name="Telefono" value="{{ $instructor->Telefono }}">
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Direccion</label>
                                    <input type="text" placeholder="Direccion" class="form-control" name="Direccion" value="{{ $instructor->Direccion }}">
                                </div> 
                        
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-sm-6">
                            <div class="form-group label-floating">              
                                <label class="control-label">Tipo de Pago</label>
                                <select name="Tipo_pago" class="form-control">
                                    <option value="{{ $instructor->Tipo_pago }}">Mensual</option>
                                    <option value="{{ $instructor->Tipo_pago }}">Comision</option>
                                </select>
                            </div>               
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group label-floating">              
                                <label class="control-label">Tipo de Disciplina</label>
                                <select name="disciplina_id" class="form-control">
                                    @foreach ($disciplinas as $disciplina)                                        
                                        <option value="{{ $disciplina->id }}" @if($disciplina->id == $instructor->disciplina_id) selected='selected' @endif>{{ $disciplina->Descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>               
                        </div>
                    </div>
                    <div class="row text-center" style="padding-top:20px;">
                        <button class="btn btn-primary">Actualizar Instructor</button>
                    </div>
                </form>
            </div>
        </div>
</div>

@endsection