@extends('home')

@section('content')

<div class="row">
        <div class="row text-center">
            <a href="{{ url('/admin/Instructor/create') }}" class="btn btn-primary">Agregar Nuevo Instructor</a>
        </div>

        <div class="row" style="padding-top:30px;">
                    <form action="{{ url('/admin/Instructor' )}}" method="get">
                        <div class="form-group">
                            <input type="text" name="search" class="form-control" placeholder="Buscar Instructor.">
                        </div>
                    </form>
        </div>

        <div class="card">        
            <div class="content table-responsive table-full-width">
                <table class="table table-striped">
                    <thead>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Tipo Pago</th>
                        <th>Porcentaje</th>
                        <th>Disciplina</th>
                        <th>Acciones</th>
                    </thead>
                    <tbody>
                        @foreach($instructors as $instructor)
                        <tr>
                            <td>{{ $instructor->id }}</td>
                            <td>{{ $instructor->Nombres }}</td>
                            <td>{{ $instructor->Apellido_Paterno }}</td>
                            <td>{{ $instructor->Apellido_Materno }}</td>
                            @if ($instructor->Tipo_pago == 1)
                                <td>Mensual</td>
                            @else
                                <td>Comision</td>
                            @endif      
                            <td>{{ $instructor->porcentaje }}</td>
                            <td>{{ $instructor->disciplina->Descripcion . ' ' . $instructor->disciplina->hora_inicio . ' ' . $instructor->disciplina->hora_fin }}</td>
                            <td>
                                <form method="post" action="{{ url('/admin/Instructor/'.$instructor->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <a href="{{ url('/admin/Instructor/'.$instructor->id) }}" title="Editar" class="btn btn-info btn-simple btn-lg">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button id="btnIncidencia" type="button" class="btn btn-danger btn-simple btn-lg" data-toggle="modal" data-target="#incidenciamodal" value="{{ $instructor->id }}" onclick="valuedeuda({{$instructor->id}})"><i class="fa fa-file"></i></button>
                                        <button type="submit" class="btn btn-danger btn-simple btn-lg"><i class="fa fa-trash"></i></button>                                        
                                </form>
                            </td>
                        </tr>                    
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

@endsection

@section('modal')
    <!-- Modal -->
    <div class="modal fade" id="incidenciamodal" tabindex="-1" role="dialog" aria-labelledby="deudaModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deudaModalLabel">Registrar Incidente del Instructor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">                    
                        <div class="content">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ url('/admin/incidencia') }}" method="post">
                                {{ csrf_field() }}                                                 
                                        <input id="hiddenincidencia" type="hidden" name="incidencia_id" value="" />                                                                                                         
                                            <div class="row" style="padding-left:100px;">                                        
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">                                            
                                                        <label class="control-label">Fecha Incidencia</label>
                                                        <input id="dateInit" type="text" class="form-control border-input" name="fecha_incidencia" value="{{ old('fecha_incidencia') }}">
                                                    </div> 
                                                </div>                                                                
                                            </div>
                                            <div class="row" style="padding-left:100px;">    
                                                <div class="col-md-6">
                                                    <textarea type="text" placeholder="Descripcion de la incidencia" class="form-control" rows="3" name="descripcion"></textarea>
                                                </div>
                                            </div>

                                            <button class="btn btn-success">Registrar Incidencia</button>                                                                                                    
                            </form>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function valuedeuda(id)
        {
            document.getElementById('hiddenincidencia').value = id
        }
        $("#dateInit").flatpickr({
            enableTime: false,
            dateFormat: "Y-m-d",
        });
    </script>
@endsection