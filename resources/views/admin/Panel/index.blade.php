@extends('home')

@section('content')

    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="card">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="icon-big icon-warning text-center">
                                <i class="ti-server"></i>
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="numbers">
                                <p>Vencidos: {{ $incripcions }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="footer"> 
                        <form action="{{ url('/admin/Panel') }}" method="post">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </form>
                    </div>
                </div>               
            </div>        
        </div>        
    </div>

@endsection