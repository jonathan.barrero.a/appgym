<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('home');
    });

    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('/admin/incripcion', 'IncripcionController@index');
    Route::get('/admin/incripcion/create', 'IncripcionController@create');
    Route::post('/admin/incripcion', 'IncripcionController@store')->name('incripcionPost');
    Route::delete('/admin/incripcion/{incripcion}', 'IncripcionController@destroy');
    Route::get('/admin/incripcion/{incripcion}', 'IncripcionController@edit');
    Route::put('/admin/incripcion/{incripcion}', 'IncripcionController@update');
    Route::get('/admin/incripcion/reportday', 'IncripcionController@reportdat');     
    Route::get('/admin/incripcion/show/day', 'IncripcionController@show');
    Route::get('/admin/incripcion/export/{dateIni}/{dateEnd}', 'IncripcionController@export');


    Route::post('/admin/incripcion/deuda', 'IncripcionController@deudacliente');
    Route::get('/admin/incripcion/vencidohoy/ver', 'IncripcionController@vencidohoy');
    Route::get('/admin/incripcion/renovar/{id}', 'IncripcionController@renovar');
    Route::post('/admin/incripcion/renovar', 'IncripcionController@renovarstore');
    Route::get('/admin/incripcion/reportcliente/{id}', 'IncripcionController@reportcliente')->name('cliente');
    Route::get('/admin/incripcion/imprimir/{id}', 'IncripcionController@imprimir')->name('imprimir');    
    Route::delete('/admin/incripcion/deuda/{id}', 'IncripcionController@destroydeuda');

    Route::get('/admin/incripcion/imprimir2/{id}', 'IncripcionController@imprimir2');   
    Route::get('/admin/incripcion/imprimir3/{id}', 'IncripcionController@imprimir3');   
    Route::get('/admin/incripcion/imprimir4/{id}', 'IncripcionController@imprimir4');
    


    Route::get('/admin/Pagos', 'PagoController@index');
    Route::get('/admin/Pagos/create', 'PagoController@create');
    Route::post('/admin/Pagos', 'PagoController@store');
    Route::delete('/admin/Pagos/{id}', 'PagoController@destroy');
    Route::get('/admin/Pagos/{pago}', 'PagoController@edit');
    Route::put('/admin/Pagos/{pago}', 'PagoController@update');


    Route::get('/admin/Instructor', 'InstructorController@index');
    Route::get('/admin/Instructor/create', 'InstructorController@create');
    Route::post('/admin/Instructor', 'InstructorController@store');
    Route::delete('/admin/Instructor/{instructor}', 'InstructorController@destroy');
    Route::get('/admin/Instructor/{instructor}', 'InstructorController@edit');
    Route::put('/admin/Instructor/{instructor}', 'InstructorController@update');
    Route::get('/admin/Instructor/show/day', 'InstructorController@show');
    Route::get('/admin/Instructor/export/{dateIni}/{dateEnd}/{id}', 'InstructorController@export');


    Route::get('/admin/disciplina', 'DisciplinaController@index');
    Route::get('/admin/disciplina/create', 'DisciplinaController@create');
    Route::post('/admin/disciplina', 'DisciplinaController@store');
    Route::delete('/admin/disciplina/{disciplina}', 'DisciplinaController@destroy');
    Route::get('/admin/disciplina/{disciplina}', 'DisciplinaController@edit');
    Route::put('/admin/disciplina/{disciplina}', 'DisciplinaController@update');

    
    Route::get('/admin/categoria', 'CategoriaController@index');
    Route::get('/admin/categoria/create', 'CategoriaController@create');
    Route::post('/admin/categoria', 'CategoriaController@store');
    Route::delete('/admin/categoria/{categoria}', 'CategoriaController@destroy');
    Route::get('/admin/categoria/{categoria}', 'CategoriaController@edit');
    Route::put('/admin/categoria/{categoria}', 'CategoriaController@update');


    Route::get('/admin/unidades', 'UnidadesController@index');
    Route::get('/admin/unidades/create', 'UnidadesController@create');
    Route::post('/admin/unidades', 'UnidadesController@store');
    Route::delete('/admin/unidades/{unidades}', 'UnidadesController@destroy');
    Route::get('/admin/unidades/{unidades}', 'UnidadesController@edit');
    Route::put('/admin/unidades/{unidades}', 'UnidadesController@update');


    Route::get('/admin/producto', 'ProductoController@index');
    Route::get('/admin/producto/create', 'ProductoController@create');
    Route::post('/admin/producto', 'ProductoController@store');
    Route::delete('/admin/producto/{producto}', 'ProductoController@destroy');
    Route::get('/admin/producto/{producto}', 'ProductoController@edit');
    Route::put('/admin/producto/{producto}', 'ProductoController@update');
    

    Route::get('/admin/ventas', 'VentaController@index');
    Route::post('/admin/ventas', 'VentaController@store');
    Route::get('/admin/ventas/show', 'VentaController@show');
    Route::post('/admin/detalleventas', 'DetalleVentaController@store');    
    Route::post('/admin/detalleventas/edit', 'DetalleVentaController@update');
    Route::delete('/admin/detalleventas/{id}', 'DetalleVentaController@destroy');
    Route::get('/admin/ventas/export/{dateIni}/{dateEnd}', 'VentaController@export');


    Route::get('/admin/compras', 'CompraController@index');
    Route::post('/admin/compras', 'CompraController@store');
    Route::get('/admin/compras/show', 'CompraController@show');
    Route::post('/admin/detallecompras', 'DetalleCompraController@store');    
    Route::post('/admin/detallecompras/edit', 'DetalleCompraController@update');
    Route::delete('/admin/detallecompras/{id}', 'DetalleCompraController@destroy');
    Route::get('/admin/compras/export/{dateIni}/{dateEnd}', 'CompraController@export');
    

    Route::get('/admin/Panel', 'PanelController@index');
    Route::post('/admin/Panel', 'PanelController@store');

    Route::get('/admin/inventario', 'InventarioController@index');
    Route::get('/admin/inventario/create', 'InventarioController@create');
    Route::post('/admin/inventario', 'InventarioController@store');
    Route::get('/admin/inventario/{inventario}', 'InventarioController@edit');
    Route::put('/admin/inventario/{inventario}', 'InventarioController@update');
    Route::delete('/admin/inventario/{inventario}', 'InventarioController@destroy');


    Route::post('/admin/incidencia', 'IncidenciaController@store');
    
});



Route::get('/admin/usuario', 'UsersController@index')->name('usuario');
