<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nro_identificacion', 50);
            $table->string('Nombre', 50);
            $table->string('Apellido_Paterno', 50);
            $table->string('Apellido_Materno', 50);
            $table->string('Telefono', 50)->nullable();
            $table->string('Direccion', 50)->nullable();
            $table->string('Email', 50)->nullable();                                    
            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
