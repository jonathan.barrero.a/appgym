<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Nombres', 50);
            $table->string('Apellido_Paterno', 50);
            $table->string('Apellido_Materno', 50);
            $table->string('Direccion', 50)->nullable();
            $table->string('Telefono', 50)->nullable();
            $table->integer('Tipo_pago');
            $table->float('porcentaje', 8, 2)->nullable();
            $table->unsignedInteger('disciplina_id');
            $table->foreign('disciplina_id')->references('id')->on('disciplinas');            

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instructors');
    }
}
